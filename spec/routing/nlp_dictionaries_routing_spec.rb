require "rails_helper"

RSpec.describe NlpDictionariesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/nlp_dictionaries").to route_to("nlp_dictionaries#index")
    end

    it "routes to #new" do
      expect(:get => "/nlp_dictionaries/new").to route_to("nlp_dictionaries#new")
    end

    it "routes to #show" do
      expect(:get => "/nlp_dictionaries/1").to route_to("nlp_dictionaries#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/nlp_dictionaries/1/edit").to route_to("nlp_dictionaries#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/nlp_dictionaries").to route_to("nlp_dictionaries#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/nlp_dictionaries/1").to route_to("nlp_dictionaries#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/nlp_dictionaries/1").to route_to("nlp_dictionaries#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/nlp_dictionaries/1").to route_to("nlp_dictionaries#destroy", :id => "1")
    end

  end
end
