require "rails_helper"

RSpec.describe EdifyResourcesController, type: :routing do
  describe "routing" do

    xit "routes to #index" do
      expect(:get => "/edify_resources").to route_to("edify_resources#index")
    end

    xit "routes to #new" do
      expect(:get => "/edify_resources/new").to route_to("edify_resources#new")
    end

    xit "routes to #show" do
      expect(:get => "/edify_resources/1").to route_to("edify_resources#show", :id => "1")
    end

    xit "routes to #edit" do
      expect(:get => "/edify_resources/1/edit").to route_to("edify_resources#edit", :id => "1")
    end

    xit "routes to #create" do
      expect(:post => "/edify_resources").to route_to("edify_resources#create")
    end

    xit "routes to #update via PUT" do
      expect(:put => "/edify_resources/1").to route_to("edify_resources#update", :id => "1")
    end

    xit "routes to #update via PATCH" do
      expect(:patch => "/edify_resources/1").to route_to("edify_resources#update", :id => "1")
    end

    xit "routes to #destroy" do
      expect(:delete => "/edify_resources/1").to route_to("edify_resources#destroy", :id => "1")
    end

  end
end
