require "rails_helper"

RSpec.describe EdifyNotificationsController, type: :routing do
  describe "routing" do

    xit "routes to #index" do
      expect(:get => "/edify_notifications").to route_to("edify_notifications#index")
    end

    xit "routes to #new" do
      expect(:get => "/edify_notifications/new").to route_to("edify_notifications#new")
    end

    xit "routes to #show" do
      expect(:get => "/edify_notifications/1").to route_to("edify_notifications#show", :id => "1")
    end

    xit "routes to #edit" do
      expect(:get => "/edify_notifications/1/edit").to route_to("edify_notifications#edit", :id => "1")
    end

    xit "routes to #create" do
      expect(:post => "/edify_notifications").to route_to("edify_notifications#create")
    end

    xit "routes to #update via PUT" do
      expect(:put => "/edify_notifications/1").to route_to("edify_notifications#update", :id => "1")
    end

    xit "routes to #update via PATCH" do
      expect(:patch => "/edify_notifications/1").to route_to("edify_notifications#update", :id => "1")
    end

    xit "routes to #destroy" do
      expect(:delete => "/edify_notifications/1").to route_to("edify_notifications#destroy", :id => "1")
    end

  end
end
