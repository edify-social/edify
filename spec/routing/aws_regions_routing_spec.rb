require "rails_helper"

RSpec.describe AwsRegionsController, type: :routing do
  describe "routing" do

    xit "routes to #index" do
      expect(:get => "/aws_regions").to route_to("aws_regions#index")
    end

    xit "routes to #new" do
      expect(:get => "/aws_regions/new").to route_to("aws_regions#new")
    end

    xit "routes to #show" do
      expect(:get => "/aws_regions/1").to route_to("aws_regions#show", :id => "1")
    end

    xit "routes to #edit" do
      expect(:get => "/aws_regions/1/edit").to route_to("aws_regions#edit", :id => "1")
    end

    xit "routes to #create" do
      expect(:post => "/aws_regions").to route_to("aws_regions#create")
    end

    xit "routes to #update via PUT" do
      expect(:put => "/aws_regions/1").to route_to("aws_regions#update", :id => "1")
    end

    xit "routes to #update via PATCH" do
      expect(:patch => "/aws_regions/1").to route_to("aws_regions#update", :id => "1")
    end

    xit "routes to #destroy" do
      expect(:delete => "/aws_regions/1").to route_to("aws_regions#destroy", :id => "1")
    end

  end
end
