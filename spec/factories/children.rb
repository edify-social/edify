# == Schema Information
#
# Table name: children
#
#  id                  :integer          not null, primary key
#  name                :string
#  phone               :string
#  imei                :string
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  pin                 :string
#  instagram           :text             default([]), is an Array
#  image               :text
#  backtrack_status    :integer          default(0), not null
#  last_device_checkin :datetime
#  device_connected    :boolean          default(TRUE)
#

FactoryGirl.define do
  factory :child do
    name                    { Faker::Name.name }
    phone                   { Faker::PhoneNumber.cell_phone }
    imei                    { Faker::Number.number(14) }
    user_id                 { Faker::Number.number(1) }
  end
end
