# == Schema Information
#
# Table name: app_lists
#
#  id           :integer          not null, primary key
#  device       :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  child_id     :integer
#  applications :string           default([]), is an Array
#

FactoryGirl.define do
  factory :app_list do
    name "MyString"
    version "MyString"
  end
end
