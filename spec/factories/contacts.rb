# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  child_id   :integer
#

FactoryGirl.define do
  factory :contact do
    content "MyText"
  end
end
