# == Schema Information
#
# Table name: device_messages
#
#  id               :integer          not null, primary key
#  message          :text
#  contact_name     :string
#  contact_number   :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  child_id         :integer
#  mobile_timestamp :string
#  sent             :boolean
#  thread_id        :integer
#  backtrack        :boolean
#  message_id       :integer
#  raw              :string
#

FactoryGirl.define do
  factory :device_message do
    message "MyText"
    contact_name "MyString"
    contact_number "MyString"
  end
end
