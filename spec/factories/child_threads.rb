# == Schema Information
#
# Table name: child_threads
#
#  id                  :integer          not null, primary key
#  child_id            :integer
#  instagram_thread_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

FactoryGirl.define do
  factory :child_thread do
    child_id 1
    instagram_thread_id 1
  end
end
