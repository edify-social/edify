# == Schema Information
#
# Table name: edify_resources
#
#  id         :integer          not null, primary key
#  link       :string
#  title      :string
#  site       :string
#  logo       :string
#  image      :string
#  brief      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :edify_resource do
    link "MyString"
    title "MyString"
    site "MyString"
    logo "MyString"
    image "MyString"
    brief "MyText"
  end
end
