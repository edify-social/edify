# == Schema Information
#
# Table name: nodes
#
#  id                      :integer          not null, primary key
#  aws_id                  :string
#  state                   :string
#  region                  :string
#  architecture            :string
#  hypervisor              :string
#  instance_type           :string
#  key_name                :string
#  launch_time             :string
#  placement               :string
#  private_dns             :string
#  private_ip              :string
#  public_dns              :string
#  public_ip               :string
#  root_device_type        :string
#  root_device_name        :string
#  state_reason            :string
#  state_transition_reason :string
#  subnet_id               :string
#  aws_tags                :text             default([]), is an Array
#  virtualization_type     :string
#  vpc_id                  :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  security_group          :text             default([]), is an Array
#  hostname                :string
#  wake_up                 :datetime
#

FactoryGirl.define do
  factory :node do
    aws_id "MyString"
    state "MyString"
    region "MyString"
    architecture "MyString"
    hypervisor "MyString"
    instance_lifecycle "MyString"
    instance_type "MyString"
    key_name "MyString"
    launch_time "MyString"
    placement "MyString"
    private_dns "MyString"
    private_ip "MyString"
    public_dns "MyString"
    public_ip "MyString"
    root_device_type "MyString"
    root_device_name "MyString"
    security_group "MyString"
    state "MyString"
    state_reason "MyString"
    state_transition_reason "MyString"
    subnet_id "MyString"
    aws_tags ""
    virtualization_type "MyString"
    vpc_id "MyString"
  end
end
