require 'rails_helper'

RSpec.describe DashboardController do
  describe 'GET mobile_apps' do
    xit 'assigns @chmonctrl' do
      childmon = ChildMonitorSetting.create(sync_time: '120')
      get :mobile_apps
      expect(assigns(:chmonctrl)).to eq([childmon])
    end

    xit 'renders the mobile_apps' do
      get :mobile_apps
      expect(response).to render_template('mobile_apps')
    end
  end
end