require 'rails_helper'

feature 'Login', feature: true do

  describe 'with basic authentication' do
    let(:user) { create(:user) }

    it 'allows basic login' do
      visit new_user_session_path
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: user.password
      click_button 'Log in'
      expect(current_path).to eq authenticated_root_path
    end

    it 'blocks invalid login' do
      visit new_user_session_path
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: 'invalid-password'
      click_button 'Log in'
      expect(page).to have_content('Invalid Email or password.')
    end
  end

  describe 'without authenticating' do
    it 'redirect to login page' do
      visit dashboard_path
      expect(page).to have_content('You need to sign in or sign up before continuing')
    end
  end

end