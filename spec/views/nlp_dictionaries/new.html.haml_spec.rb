require 'rails_helper'

RSpec.describe "nlp_dictionaries/new", type: :view do
  before(:each) do
    assign(:nlp_dictionary, NlpDictionary.new(
      :content => "MyText",
      :label => "MyString"
    ))
  end

  it "renders new nlp_dictionary form" do
    render

    assert_select "form[action=?][method=?]", nlp_dictionaries_path, "post" do

      assert_select "textarea#nlp_dictionary_content[name=?]", "nlp_dictionary[content]"

      assert_select "input#nlp_dictionary_label[name=?]", "nlp_dictionary[label]"
    end
  end
end
