require 'rails_helper'

RSpec.describe "nlp_dictionaries/show", type: :view do
  before(:each) do
    @nlp_dictionary = assign(:nlp_dictionary, NlpDictionary.create!(
      :content => "MyText",
      :label => "Label"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Label/)
  end
end
