require 'rails_helper'

RSpec.describe "nlp_dictionaries/index", type: :view do
  before(:each) do
    assign(:nlp_dictionaries, [
      NlpDictionary.create!(
        :content => "MyText",
        :label => "Label"
      ),
      NlpDictionary.create!(
        :content => "MyText",
        :label => "Label"
      )
    ])
  end

  it "renders a list of nlp_dictionaries" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Label".to_s, :count => 2
  end
end
