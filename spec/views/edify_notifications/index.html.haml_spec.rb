require 'rails_helper'

RSpec.describe "edify_notifications/index", type: :view do
  before(:each) do
    assign(:edify_notifications, [
      EdifyNotification.create!(
        :msg_id => 2,
        :content => "MyText",
        :user => "User",
        :label => "Label",
        :status => "Status",
        :type => "Type",
        :channel => "Channel",
        :feedback => "Feedback"
      ),
      EdifyNotification.create!(
        :msg_id => 2,
        :content => "MyText",
        :user => "User",
        :label => "Label",
        :status => "Status",
        :type => "Type",
        :channel => "Channel",
        :feedback => "Feedback"
      )
    ])
  end

  xit "renders a list of edify_notifications" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "User".to_s, :count => 2
    assert_select "tr>td", :text => "Label".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => "Channel".to_s, :count => 2
    assert_select "tr>td", :text => "Feedback".to_s, :count => 2
  end
end
