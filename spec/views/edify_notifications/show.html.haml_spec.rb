require 'rails_helper'

RSpec.describe "edify_notifications/show", type: :view do
  before(:each) do
    @edify_notification = assign(:edify_notification, EdifyNotification.create!(
      :msg_id => 2,
      :content => "MyText",
      :user => "User",
      :label => "Label",
      :status => "Status",
      :type => "Type",
      :channel => "Channel",
      :feedback => "Feedback"
    ))
  end

  xit "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/User/)
    expect(rendered).to match(/Label/)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/Type/)
    expect(rendered).to match(/Channel/)
    expect(rendered).to match(/Feedback/)
  end
end
