require 'rails_helper'

RSpec.describe "edify_notifications/edit", type: :view do
  before(:each) do
    @edify_notification = assign(:edify_notification, EdifyNotification.create!(
      :content => "MyText",
      :user => "MyString",
      :label => "MyString",
      :status => "MyString",
      :notification_type => "",
      :channel => "MyString",
      :feedback => "MyString"
    ))
  end

  it "renders the edit edify_notification form" do
    render

    assert_select "form[action=?][method=?]", edify_notification_path(@edify_notification), "post" do

      # assert_select "input#edify_notification_msg_id[name=?]", "edify_notification[msg_id]"

      assert_select "textarea#edify_notification_content[name=?]", "edify_notification[content]"

      assert_select "input#edify_notification_user[name=?]", "edify_notification[user]"

      assert_select "input#edify_notification_label[name=?]", "edify_notification[label]"

      assert_select "input#edify_notification_status[name=?]", "edify_notification[status]"

      # assert_select "input#edify_notification_type[name=?]", "edify_notification[notification_type]"

      assert_select "input#edify_notification_channel[name=?]", "edify_notification[channel]"

      assert_select "input#edify_notification_feedback[name=?]", "edify_notification[feedback]"
    end
  end
end
