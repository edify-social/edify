require 'rails_helper'

RSpec.describe "edify_resources/show", type: :view do
  before(:each) do
    @edify_resource = assign(:edify_resource, EdifyResource.create!(
      :link => "Link",
      :title => "Title",
      :site => "Site",
      :logo => "Logo",
      :image => "Image",
      :brief => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Link/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Site/)
    expect(rendered).to match(/Logo/)
    expect(rendered).to match(/Image/)
    expect(rendered).to match(/MyText/)
  end
end
