require 'rails_helper'

RSpec.describe "edify_resources/new", type: :view do
  before(:each) do
    assign(:edify_resource, EdifyResource.new(
      :link => "MyString",
      :title => "MyString",
      :site => "MyString",
      :logo => "MyString",
      :image => "MyString",
      :brief => "MyText"
    ))
  end

  it "renders new edify_resource form" do
    render

    assert_select "form[action=?][method=?]", edify_resources_path, "post" do

      assert_select "input#edify_resource_link[name=?]", "edify_resource[link]"

      assert_select "input#edify_resource_title[name=?]", "edify_resource[title]"

      assert_select "input#edify_resource_site[name=?]", "edify_resource[site]"

      assert_select "input#edify_resource_logo[name=?]", "edify_resource[logo]"

      assert_select "input#edify_resource_image[name=?]", "edify_resource[image]"

      assert_select "textarea#edify_resource_brief[name=?]", "edify_resource[brief]"
    end
  end
end
