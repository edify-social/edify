require 'rails_helper'

RSpec.describe "edify_resources/index", type: :view do
  before(:each) do
    assign(:edify_resources, [
      EdifyResource.create!(
        :link => "Link",
        :title => "Title",
        :site => "Site",
        :logo => "Logo",
        :image => "Image",
        :brief => "MyText"
      ),
      EdifyResource.create!(
        :link => "Link",
        :title => "Title",
        :site => "Site",
        :logo => "Logo",
        :image => "Image",
        :brief => "MyText"
      )
    ])
  end

  xit "renders a list of edify_resources" do
    render
    assert_select "tr>td", :text => "Link".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Site".to_s, :count => 2
    assert_select "tr>td", :text => "Logo".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
