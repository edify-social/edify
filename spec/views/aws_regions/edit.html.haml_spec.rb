require 'rails_helper'

RSpec.describe "aws_regions/edit", type: :view do
  before(:each) do
    @aws_region = assign(:aws_region, AwsRegion.create!(
      :name => "MyString",
      :code => "MyString",
      :active => false
    ))
  end

  it "renders the edit aws_region form" do
    render

    assert_select "form[action=?][method=?]", aws_region_path(@aws_region), "post" do

      assert_select "input#aws_region_name[name=?]", "aws_region[name]"

      assert_select "input#aws_region_code[name=?]", "aws_region[code]"

      assert_select "input#aws_region_active[name=?]", "aws_region[active]"
    end
  end
end
