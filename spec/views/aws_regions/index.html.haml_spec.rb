require 'rails_helper'

RSpec.describe "aws_regions/index", type: :view do
  before(:each) do
    assign(:aws_regions, [
      AwsRegion.create!(
        :name => "Name",
        :code => "Code",
        :active => false
      ),
      AwsRegion.create!(
        :name => "Name",
        :code => "Code",
        :active => false
      )
    ])
  end

  it "renders a list of aws_regions" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
