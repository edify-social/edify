require 'rails_helper'

RSpec.describe "aws_regions/new", type: :view do
  before(:each) do
    assign(:aws_region, AwsRegion.new(
      :name => "MyString",
      :code => "MyString",
      :active => false
    ))
  end

  it "renders new aws_region form" do
    render

    assert_select "form[action=?][method=?]", aws_regions_path, "post" do

      assert_select "input#aws_region_name[name=?]", "aws_region[name]"

      assert_select "input#aws_region_code[name=?]", "aws_region[code]"

      assert_select "input#aws_region_active[name=?]", "aws_region[active]"
    end
  end
end
