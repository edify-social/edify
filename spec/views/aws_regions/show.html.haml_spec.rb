require 'rails_helper'

RSpec.describe "aws_regions/show", type: :view do
  before(:each) do
    @aws_region = assign(:aws_region, AwsRegion.create!(
      :name => "Name",
      :code => "Code",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Code/)
    expect(rendered).to match(/false/)
  end
end
