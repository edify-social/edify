# == Schema Information
#
# Table name: child_monitor_settings
#
#  id         :integer          not null, primary key
#  sync_time  :string           default("3600")
#  user       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe ChildMonitorSetting, type: :model do
  it 'is valid with valid attributes' do
    control = ChildMonitorSetting.new
    control.sync_time = '100'
    control.user = 'John'
    expect(control).to be_valid
  end

  it 'is not valid without sync_time' do
    control = ChildMonitorSetting.new(sync_time: nil)
    expect(control).not_to be_valid
  end
end
