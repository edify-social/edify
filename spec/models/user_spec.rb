# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  admin                  :boolean
#  authentication_token   :string
#  avatar                 :string
#  phone                  :string
#  player_id              :string
#  push_token             :string
#  backtrack              :boolean
#

require 'rails_helper'

RSpec.describe User, type: :model do

  it 'has a valid factory' do
    user = create :user
    expect(user).to be_valid
  end

end
