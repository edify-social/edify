# == Schema Information
#
# Table name: thread_messages
#
#  id                   :integer          not null, primary key
#  instagram_message_id :integer
#  instagram_user_id    :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe ThreadMessage, type: :model do
  it { is_expected.to validate_presence_of(:instagram_message_id) }
  it { is_expected.to validate_presence_of(:instagram_user_id) }
end
