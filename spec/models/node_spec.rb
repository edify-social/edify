# == Schema Information
#
# Table name: nodes
#
#  id                      :integer          not null, primary key
#  aws_id                  :string
#  state                   :string
#  region                  :string
#  architecture            :string
#  hypervisor              :string
#  instance_type           :string
#  key_name                :string
#  launch_time             :string
#  placement               :string
#  private_dns             :string
#  private_ip              :string
#  public_dns              :string
#  public_ip               :string
#  root_device_type        :string
#  root_device_name        :string
#  state_reason            :string
#  state_transition_reason :string
#  subnet_id               :string
#  aws_tags                :text             default([]), is an Array
#  virtualization_type     :string
#  vpc_id                  :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  security_group          :text             default([]), is an Array
#  hostname                :string
#  wake_up                 :datetime
#

require 'rails_helper'

RSpec.describe Node, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
