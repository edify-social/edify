# == Schema Information
#
# Table name: children
#
#  id                  :integer          not null, primary key
#  name                :string
#  phone               :string
#  imei                :string
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  pin                 :string
#  instagram           :text             default([]), is an Array
#  image               :text
#  backtrack_status    :integer          default(0), not null
#  last_device_checkin :datetime
#  device_connected    :boolean          default(TRUE)
#

require 'rails_helper'

RSpec.describe Child, type: :model do

  it 'has a valid factory' do
    child = create :child
    expect(child).to be_valid
  end
  it 'is valid with valid attributes' do
    child         = Child.new
    child.name    = 'Kid'
    child.phone   = '87654321'
    child.imei    = '876543223456'
    child.user_id = 23
    expect(child).to be_valid
  end

  it 'is not valid without a name' do
    child = Child.new(name: nil)
    expect(child).not_to be_valid
  end

  it 'is not valid without a user_id' do
    child = Child.new(user_id: nil)
    expect(child).not_to be_valid
  end

end
