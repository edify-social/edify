# == Schema Information
#
# Table name: recipients_assocs
#
#  id                  :integer          not null, primary key
#  instagram_user_id   :integer
#  instagram_thread_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'rails_helper'

RSpec.describe RecipientsAssoc, type: :model do
  it { is_expected.to validate_presence_of(:instagram_user_id) }
  it { is_expected.to validate_presence_of(:instagram_thread_id) }
end
