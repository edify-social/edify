# == Schema Information
#
# Table name: instagram_threads
#
#  id            :integer          not null, primary key
#  thread_id     :string
#  thread_title  :string
#  string        :string
#  muted         :boolean
#  named         :boolean
#  canonical     :boolean
#  last_activity :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe InstagramThread, type: :model do
  it { is_expected.to validate_presence_of(:thread_id) }  
  it { is_expected.to validate_uniqueness_of(:thread_id) }
end
