source 'https://rubygems.org'

ruby '2.3.1'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
# Forms
gem 'simple_form'
# Authentication
gem 'devise'
# HTML parser
gem 'haml-rails', '~> 0.9'
# Annotate models
gem 'annotate'

# Sidekiq Dashboard
gem 'sinatra', require: false
# Background jobs
gem 'sidekiq', '3.3.0'
# scheduling sidekiq jobs
gem 'sidekiq-scheduler', '~> 1'
# Sidekiq status
gem 'sidekiq_status'

# Token Auth
gem 'simple_token_authentication'
# Omniauth
gem 'omniauth', '~> 1.3', '>= 1.3.1'
# CORS
gem 'rack-cors', :require => 'rack/cors'
# Icons
gem 'font-awesome-rails'

# Tags
gem 'acts-as-taggable-on', '~> 4.0'

# NLP
gem 'monkeylearn'
gem 'rosette_api'

# Chef and Knife management
gem 'ridley', '~> 5.1'
gem 'knife-api'

# AWS integration
gem 'aws-sdk-rails'

# Deployments
group :development do
  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
end

# Server
gem 'puma'

# An interface to the ImageMagick and GraphicsMagick image processing libraries. http://rmagick.rubyforge.org/
gem 'rmagick', :require => false
# Classier solution for file uploads for Rails, Sinatra and other Ruby web frameworks
gem 'carrierwave'
# Base 64 uploads
gem 'carrierwave-base64'
# Pagination
gem 'kaminari'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

group :test do
  gem 'shoulda-matchers', '~> 3.0', require: false
end

group :development, :test do
  # Clean db between tests or rake tasks
  gem 'database_cleaner', '~> 1.5'
  # Testing framework
  gem 'rspec-rails', '~> 3.5'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Coverage
  gem 'simplecov', require: false
  # Static code analyzer
  gem 'rubocop', require: false
  # Haml validator
  gem 'haml-lint', require: false
  # Functional tests
  gem 'capybara', '~> 2.5'
  # Sample data
  gem 'faker', '~> 1.6.1'
  # Test objects
  gem 'factory_girl_rails', '~> 4.7.0'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end
