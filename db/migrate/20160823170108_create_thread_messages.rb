class CreateThreadMessages < ActiveRecord::Migration
  def change
    create_table :thread_messages do |t|
      t.integer :instagram_message_id
      t.integer :instagram_user_id

      t.timestamps null: false
    end
  end
end
