class CreateNodes < ActiveRecord::Migration
  def change
    create_table :nodes do |t|
      t.string :aws_id
      t.string :state
      t.string :region
      t.string :architecture
      t.string :hypervisor
      t.string :instance_lifecycle
      t.string :instance_type
      t.string :key_name
      t.string :launch_time
      t.string :placement
      t.string :private_dns
      t.string :private_ip
      t.string :public_dns
      t.string :public_ip
      t.string :root_device_type
      t.string :root_device_name
      t.string :security_group
      t.string :state
      t.string :state_reason
      t.string :state_transition_reason
      t.string :subnet_id
      t.text   :aws_tags, array:true, default: []
      t.string :virtualization_type
      t.string :vpc_id

      t.timestamps null: false
    end
  end
end
