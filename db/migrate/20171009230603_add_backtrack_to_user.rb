class AddBacktrackToUser < ActiveRecord::Migration
  def change
    add_column :users, :backtrack, :boolean
  end
end
