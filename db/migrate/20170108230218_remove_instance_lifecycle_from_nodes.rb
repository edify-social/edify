class RemoveInstanceLifecycleFromNodes < ActiveRecord::Migration
  def change
    remove_column :nodes, :instance_lifecycle, :string
  end
end
