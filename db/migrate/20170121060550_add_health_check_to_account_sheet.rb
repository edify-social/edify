class AddHealthCheckToAccountSheet < ActiveRecord::Migration
  def change
    add_column :account_sheets, :health_check, :string
  end
end
