class AddChildIdToDeviceMessages < ActiveRecord::Migration
  def change
    add_column :device_messages, :child_id, :integer
  end
end
