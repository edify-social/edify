class CreateChildMonitorSettings < ActiveRecord::Migration
  def change
    create_table :child_monitor_settings do |t|
      t.string :sync_time
      t.string :user

      t.timestamps null: false
    end
  end
end
