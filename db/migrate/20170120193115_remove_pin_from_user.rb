class RemovePinFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :pin, :string
  end
end
