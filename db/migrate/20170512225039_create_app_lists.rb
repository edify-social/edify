class CreateAppLists < ActiveRecord::Migration
  def change
    create_table :app_lists do |t|
      t.string :name
      t.string :version

      t.timestamps null: false
    end
  end
end
