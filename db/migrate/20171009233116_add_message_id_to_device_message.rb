class AddMessageIdToDeviceMessage < ActiveRecord::Migration
  def change
    add_column :device_messages, :message_id, :integer
  end
end
