class CreateInstagramMessages < ActiveRecord::Migration
  def change
    create_table :instagram_messages do |t|
      t.string :content_type
      t.string :status
      t.string :item_type
      t.string :item_id
      t.string :client_context
      t.string :timestamp
      t.string :timestamp_in_micro
      t.string :uid
      t.text :text
      t.string :preview_medias
      t.string :action_log
      t.text :thread_key, array: true, default: []
      t.boolean :hide_in_thread
      t.integer :instagram_thread_id

      t.timestamps null: false
    end
  end
end
