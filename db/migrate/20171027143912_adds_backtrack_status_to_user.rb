class AddsBacktrackStatusToUser < ActiveRecord::Migration
  def change
    add_column :users, :backtrack_status, :integer, null: false, default: 0
  end
end
