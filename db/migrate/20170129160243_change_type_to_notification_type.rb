class ChangeTypeToNotificationType < ActiveRecord::Migration
  def change
    rename_column :edify_notifications, :type, :notification_type
  end
end
