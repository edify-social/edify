class AddRawToDeviceMessages < ActiveRecord::Migration
  def change
    add_column :device_messages, :raw, :string
  end
end
