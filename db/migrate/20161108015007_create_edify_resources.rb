class CreateEdifyResources < ActiveRecord::Migration
  def change
    create_table :edify_resources do |t|
      t.string :link
      t.string :title
      t.string :site
      t.string :logo
      t.string :image
      t.text :brief

      t.timestamps null: false
    end
  end
end
