class CreateInstagramThreads < ActiveRecord::Migration
  def change
    create_table :instagram_threads do |t|
      t.string :thread_id
      t.string :thread_title
      t.string :string
      t.boolean :muted
      t.boolean :named
      t.boolean :canonical
      t.string :last_activity
      t.integer :child_id

      t.timestamps null: false
    end
  end
end
