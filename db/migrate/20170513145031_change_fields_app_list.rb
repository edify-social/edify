class ChangeFieldsAppList < ActiveRecord::Migration
  def change
    rename_column :app_lists, :name, :applications
    rename_column :app_lists, :version, :device
  end
end
