class AddDeviceConnectedToChild < ActiveRecord::Migration
  def change
    add_column :children, :device_connected, :boolean, default: true
  end
end
