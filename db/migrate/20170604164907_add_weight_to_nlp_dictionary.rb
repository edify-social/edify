class AddWeightToNlpDictionary < ActiveRecord::Migration
  def change
    add_column :nlp_dictionaries, :weight, :decimal
  end
end
