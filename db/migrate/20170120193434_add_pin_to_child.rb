class AddPinToChild < ActiveRecord::Migration
  def change
    add_column :children, :pin, :string
  end
end
