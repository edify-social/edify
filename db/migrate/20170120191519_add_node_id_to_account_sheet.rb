class AddNodeIdToAccountSheet < ActiveRecord::Migration
  def change
    add_column :account_sheets, :node_id, :integer
  end
end
