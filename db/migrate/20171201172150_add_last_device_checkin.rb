class AddLastDeviceCheckin < ActiveRecord::Migration
  def change
    add_column :children, :last_device_checkin, :datetime
  end
end
