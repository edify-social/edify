class CreateNlpDictionaries < ActiveRecord::Migration
  def change
    create_table :nlp_dictionaries do |t|
      t.text :content
      t.string :label

      t.timestamps null: false
    end
  end
end
