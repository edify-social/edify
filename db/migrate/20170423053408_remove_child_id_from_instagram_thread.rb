class RemoveChildIdFromInstagramThread < ActiveRecord::Migration
  def change
    remove_column :instagram_threads, :child_id
  end
end
