class AddSentimentToInstagramMessages < ActiveRecord::Migration
  def change
    add_column :instagram_messages, :sentiment, :string
  end
end
