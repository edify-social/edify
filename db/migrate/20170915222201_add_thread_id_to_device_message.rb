class AddThreadIdToDeviceMessage < ActiveRecord::Migration
  def change
    add_column :device_messages, :thread_id, :integer
  end
end
