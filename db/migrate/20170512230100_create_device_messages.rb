class CreateDeviceMessages < ActiveRecord::Migration
  def change
    create_table :device_messages do |t|
      t.text :message
      t.string :contact_name
      t.string :contact_number

      t.timestamps null: false
    end
  end
end
