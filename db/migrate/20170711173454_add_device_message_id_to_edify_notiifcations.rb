class AddDeviceMessageIdToEdifyNotiifcations < ActiveRecord::Migration
  def change
    add_column :edify_notifications, :device_message_id, :integer
  end
end
