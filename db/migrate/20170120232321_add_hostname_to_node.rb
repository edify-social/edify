class AddHostnameToNode < ActiveRecord::Migration
  def change
    add_column :nodes, :hostname, :string
  end
end
