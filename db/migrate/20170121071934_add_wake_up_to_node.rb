class AddWakeUpToNode < ActiveRecord::Migration
  def change
    add_column :nodes, :wake_up, :datetime
  end
end
