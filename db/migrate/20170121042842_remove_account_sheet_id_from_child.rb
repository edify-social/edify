class RemoveAccountSheetIdFromChild < ActiveRecord::Migration
  def change
    remove_column :children, :account_sheet_id, :integer
  end
end
