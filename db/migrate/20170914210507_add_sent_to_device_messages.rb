class AddSentToDeviceMessages < ActiveRecord::Migration
  def change
    add_column :device_messages, :sent, :boolean
  end
end
