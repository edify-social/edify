class ChangeCollumnSecurityGroupFromNodes < ActiveRecord::Migration
  def change
    remove_column :nodes, :security_group
    add_column :nodes, :security_group, :text, array: true , default: []
  end
end
