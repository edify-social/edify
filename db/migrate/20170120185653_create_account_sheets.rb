class CreateAccountSheets < ActiveRecord::Migration
  def change
    create_table :account_sheets do |t|
      t.string :pin
      t.string :last_run
      t.string :status
      t.text   :instagram_account, array:true, default: []
      t.string :server
      t.string :emulator

      t.timestamps null: false
    end
  end
end
