class RemoveBacktrackStatusFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :backtrack_status, :integer
  end
end
