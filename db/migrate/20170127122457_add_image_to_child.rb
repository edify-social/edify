class AddImageToChild < ActiveRecord::Migration
  def change
    add_column :children, :image, :text
  end
end
