class AddAccountSheetIdToChild < ActiveRecord::Migration
  def change
    add_column :children, :account_sheet_id, :integer
  end
end
