class AddPushIdsToUser < ActiveRecord::Migration
  def change
    add_column :users, :player_id, :string
    add_column :users, :push_token, :string
  end
end
