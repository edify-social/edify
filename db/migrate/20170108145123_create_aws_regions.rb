class CreateAwsRegions < ActiveRecord::Migration
  def change
    create_table :aws_regions do |t|
      t.string :name
      t.string :code
      t.boolean :active

      t.timestamps null: false
    end
  end
end
