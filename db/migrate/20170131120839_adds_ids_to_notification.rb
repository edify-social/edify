class AddsIdsToNotification < ActiveRecord::Migration
  def change
    remove_column :edify_notifications, :msg_id
    add_column :edify_notifications, :is_active, :boolean, default: true
    add_column :edify_notifications, :instagram_message_id, :integer
    add_column :edify_notifications, :child_id, :integer
  end
end
