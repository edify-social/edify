class AddMobileTimestampToDeviceMessages < ActiveRecord::Migration
  def change
    add_column :device_messages, :mobile_timestamp, :string
  end
end
