class CreateInstagramUsers < ActiveRecord::Migration
  def change
    create_table :instagram_users do |t|
      t.string :username
      t.string :full_name
      t.string :profile_pic_url
      t.boolean :has_anonymous_profile_picture
      t.string :uid
      t.boolean :is_geo_ip_blocked
      t.boolean :is_staff
      t.boolean :usertag_review_enabled
      t.text :biography
      t.string :external_lynx_url
      t.string :external_url
      t.integer :follower_count
      t.integer :following_count
      t.integer :media_count
      t.boolean :is_private
      t.integer :geo_media_count
      t.integer :usertags_count
      t.boolean :is_verified
      t.string :byline
      t.string :chaining_suggestions
      t.integer :coeff_weight
      t.boolean :can_see_organic_insights
      t.integer :inviter_id
      t.integer :recipient_id
      t.integer :instagram_message_id

      t.timestamps null: false
    end
  end
end
