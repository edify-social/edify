class AddChildIdToAppList < ActiveRecord::Migration
  def change
    add_column :app_lists, :child_id, :integer
  end
end
