class CreateEdifyNotifications < ActiveRecord::Migration
  def change
    create_table :edify_notifications do |t|
      t.integer :msg_id
      t.text :content
      t.string :user
      t.string :label
      t.string :status
      t.string :type
      t.string :channel
      t.string :feedback

      t.timestamps null: false
    end
  end
end
