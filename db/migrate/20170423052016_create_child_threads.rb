class CreateChildThreads < ActiveRecord::Migration
  def change
    create_table :child_threads do |t|
      t.integer :child_id
      t.integer :instagram_thread_id

      t.timestamps null: false
    end
  end
end
