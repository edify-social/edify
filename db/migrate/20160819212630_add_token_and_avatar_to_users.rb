class AddTokenAndAvatarToUsers < ActiveRecord::Migration
  def change
    add_column :users, :authentication_token, :string
    add_column :users, :avatar, :string
  end
end
