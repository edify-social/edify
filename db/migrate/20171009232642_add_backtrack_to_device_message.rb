class AddBacktrackToDeviceMessage < ActiveRecord::Migration
  def change
    add_column :device_messages, :backtrack, :boolean
  end
end
