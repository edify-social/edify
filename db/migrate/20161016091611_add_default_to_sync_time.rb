class AddDefaultToSyncTime < ActiveRecord::Migration
  def change
    change_column_default :child_monitor_settings, :sync_time, '3600'
  end
end
