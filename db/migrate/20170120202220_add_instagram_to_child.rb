class AddInstagramToChild < ActiveRecord::Migration
  def change
    add_column :children, :instagram, :text, array:true, default: []
  end
end
