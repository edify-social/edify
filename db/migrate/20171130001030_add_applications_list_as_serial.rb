class AddApplicationsListAsSerial < ActiveRecord::Migration
  def change
    remove_column :app_lists, :applications
    add_column :app_lists, :applications, :string, array: true, default: []
  end
end
