class CreateRecipientsAssocs < ActiveRecord::Migration
  def change
    create_table :recipients_assocs do |t|
      t.integer :instagram_user_id
      t.integer :instagram_thread_id

      t.timestamps null: false
    end
  end
end
