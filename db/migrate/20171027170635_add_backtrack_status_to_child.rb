class AddBacktrackStatusToChild < ActiveRecord::Migration
  def change
    add_column :children, :backtrack_status, :integer, null: false, default: 0
  end
end
