# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171210112244) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_sheets", force: :cascade do |t|
    t.string   "pin"
    t.string   "last_run"
    t.string   "status"
    t.text     "instagram_account", default: [],              array: true
    t.string   "server"
    t.string   "emulator"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "node_id"
    t.integer  "child_id"
    t.string   "health_check"
  end

  create_table "app_lists", force: :cascade do |t|
    t.string   "device"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "child_id"
    t.string   "applications", default: [],              array: true
  end

  create_table "aws_regions", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "child_monitor_controls", force: :cascade do |t|
    t.string   "sync_time",  default: "3600"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "user"
  end

  create_table "child_monitor_settings", force: :cascade do |t|
    t.string   "sync_time",  default: "3600"
    t.string   "user"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "child_threads", force: :cascade do |t|
    t.integer  "child_id"
    t.integer  "instagram_thread_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "children", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "imei"
    t.integer  "user_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "pin"
    t.text     "instagram",           default: [],                array: true
    t.text     "image"
    t.integer  "backtrack_status",    default: 0,    null: false
    t.datetime "last_device_checkin"
    t.boolean  "device_connected",    default: true
  end

  create_table "contacts", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "child_id"
  end

  create_table "device_messages", force: :cascade do |t|
    t.text     "message"
    t.string   "contact_name"
    t.string   "contact_number"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "child_id"
    t.string   "mobile_timestamp"
    t.boolean  "sent"
    t.integer  "thread_id"
    t.boolean  "backtrack"
    t.integer  "message_id"
    t.string   "raw"
  end

  create_table "edify_notifications", force: :cascade do |t|
    t.text     "content"
    t.string   "user"
    t.string   "label"
    t.string   "status"
    t.string   "notification_type"
    t.string   "channel"
    t.string   "feedback"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "is_active",            default: true
    t.integer  "instagram_message_id"
    t.integer  "child_id"
    t.integer  "device_message_id"
  end

  create_table "edify_resources", force: :cascade do |t|
    t.string   "link"
    t.string   "title"
    t.string   "site"
    t.string   "logo"
    t.string   "image"
    t.text     "brief"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "instagram_messages", force: :cascade do |t|
    t.string   "content_type"
    t.string   "status"
    t.string   "item_type"
    t.string   "item_id"
    t.string   "client_context"
    t.string   "timestamp"
    t.string   "timestamp_in_micro"
    t.string   "uid"
    t.text     "text"
    t.string   "preview_medias"
    t.string   "action_log"
    t.text     "thread_key",          default: [],              array: true
    t.boolean  "hide_in_thread"
    t.integer  "instagram_thread_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "sentiment"
  end

  create_table "instagram_threads", force: :cascade do |t|
    t.string   "thread_id"
    t.string   "thread_title"
    t.string   "string"
    t.boolean  "muted"
    t.boolean  "named"
    t.boolean  "canonical"
    t.string   "last_activity"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "instagram_users", force: :cascade do |t|
    t.string   "username"
    t.string   "full_name"
    t.string   "profile_pic_url"
    t.boolean  "has_anonymous_profile_picture"
    t.string   "uid"
    t.boolean  "is_geo_ip_blocked"
    t.boolean  "is_staff"
    t.boolean  "usertag_review_enabled"
    t.text     "biography"
    t.string   "external_lynx_url"
    t.string   "external_url"
    t.integer  "follower_count"
    t.integer  "following_count"
    t.integer  "media_count"
    t.boolean  "is_private"
    t.integer  "geo_media_count"
    t.integer  "usertags_count"
    t.boolean  "is_verified"
    t.string   "byline"
    t.string   "chaining_suggestions"
    t.integer  "coeff_weight"
    t.boolean  "can_see_organic_insights"
    t.integer  "inviter_id"
    t.integer  "recipient_id"
    t.integer  "instagram_message_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "nlp_dictionaries", force: :cascade do |t|
    t.text     "content"
    t.string   "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal  "weight"
  end

  create_table "nodes", force: :cascade do |t|
    t.string   "aws_id"
    t.string   "state"
    t.string   "region"
    t.string   "architecture"
    t.string   "hypervisor"
    t.string   "instance_type"
    t.string   "key_name"
    t.string   "launch_time"
    t.string   "placement"
    t.string   "private_dns"
    t.string   "private_ip"
    t.string   "public_dns"
    t.string   "public_ip"
    t.string   "root_device_type"
    t.string   "root_device_name"
    t.string   "state_reason"
    t.string   "state_transition_reason"
    t.string   "subnet_id"
    t.text     "aws_tags",                default: [],              array: true
    t.string   "virtualization_type"
    t.string   "vpc_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.text     "security_group",          default: [],              array: true
    t.string   "hostname"
    t.datetime "wake_up"
  end

  create_table "recipients_assocs", force: :cascade do |t|
    t.integer  "instagram_user_id"
    t.integer  "instagram_thread_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["context"], name: "index_taggings_on_context", using: :btree
  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
  add_index "taggings", ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
  add_index "taggings", ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
  add_index "taggings", ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
  add_index "taggings", ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "thread_messages", force: :cascade do |t|
    t.integer  "instagram_message_id"
    t.integer  "instagram_user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.boolean  "admin"
    t.string   "authentication_token"
    t.string   "avatar"
    t.string   "phone"
    t.string   "player_id"
    t.string   "push_token"
    t.boolean  "backtrack"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
