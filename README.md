# Edify Backend  

### Review Code ToDos
* List al todos: `rake notes:todo`

### Redis
* Stop `sudo service redis-server stop`
* Status `sudo service redis-server status`
* Start `sudo service redis-server start`

### Sidekiq
* Sidekiq manages background jobs
* Make sure to restart Sidekiq after editing a worker
* Start sidekiq in prod with `bundle exec sidekiq -d -L log/sidekiq.log -C config/sidekiq.yml -e production` 
executed from `apps/edify/current/`
* Stop Sidekiq by looking for live processes with `ps aux | grep sidekiq` and
then killing them with `kill -9 <pid>`

### API
* Our API follows Swagger standards
* You can try out the calls by expanding each section and pressing the
`Try it out` button.

### CI/CD
* Install the runner with [GitLab CI](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md)
* Make sure the Runner is active before using CI

## Deploy

* All changes on the `staging` remote branch will be published
* To deploy make sure `staging` is up to date and run `cap staging deploy`

## Production

* The production environment requires the Chef DK installed
* Follow [this](https://www.linode.com/docs/applications/chef/deploy-a-chef-server-workstation-and-node-on-ubuntu-14-04) tutorial but change the versions with latest packages
* Download latest Chef DK from https://downloads.chef.io/chefdk
* Using either Chef or Knife API requires the `.chef` directory with its `knife` file and `pem`.

## Notes

* Using Sidekiq 3.3.0 because Ridley is only compatible with Celluloid 16.0.
* This might be removed if Knife api is enough to manage Nodes.
* If we don't need Ridley then remove the Ridley gem and run `bundle update sidekiq`
* Sidekiq is 3.3.0 and was 3.5.4. Updating would bring celluloid-io back to 0.17.3 from 0.16.2
* Remember that the `knife.rb` is specific to an organization
* Stupid error installing rmagick on Sierra: http://stackoverflow.com/a/41788501/4059386
* Stupid error installing nokogiri http://stackoverflow.com/questions/5528839/why-does-installing-nokogiri-on-mac-os-fail-with-libiconv-is-missing
