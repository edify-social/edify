class DashboardController < ApplicationController
  respond_to :html, :js

  def show
    # TODO: scope if not admin
    @users = User.all
    @children = Child.all
    @insta_messages = InstagramMessage.all
    @insta_users = InstagramUser.all
  end

  def notify
    dmsg = DeviceMessage.find(params[:dm_id])
    chid = params[:chid]
    chld = Child.find(chid)

    params = {
      app_id: '8c010546-76ff-40f6-8761-5163eb01f8dd',
      contents:
        {
          en: 'You have a new notification'
        },
      ios_badgeType: 'Increase',
      ios_badgeCount: 1,
      include_player_ids: [chld.user.player_id]
    }
    uri = URI.parse('https://onesignal.com/api/v1/notifications')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request = Net::HTTP::Post.new(uri.path,
                                  'Content-Type'  => 'application/json;charset=utf-8',
                                  'Authorization' => 'Basic ZGM4MDUxMjgtMzZlMi00MjJkLWE4NjgtZTNjYTc2YjVjMTc4')
    request.body = params.as_json.to_json
    response = http.request(request)
    puts response.body

    EdifyNotification.create(
      child_id: chid,
      channel: 'SMS',
      device_message_id: dmsg.id,
      notification_type: 'Device',
      label: 'Negative',
      content: dmsg.message,
      user: chld.user
    )

    #dmsg << ed
    #dmsg.save
    redirect_to child_path, id: chid
  end

=begin
  def notify
    imsg = InstagramMessage.find(params[:im_id])
    chid = params[:chid]
    th = params[:th_id]
    instusr = InstagramUser.find_by_uid(imsg.uid)

    # Get user
    if instusr
      usr = instusr.username
    else
      usr = imsg.id
    end

    # Get content
    if imsg.text
      cnt = imsg.text
    else
      cnt = imsg.content_type
    end
    # Assign to child by creating a notification
    # TODO: (Notif_type) Pipe in other types when we figure out where they live
    # TODO: (label) The label should come in from NLP already
    # TODO: Confirm if user is instagram user
    EdifyNotification.create(
        child_id: chid,
        instagram_message_id: imsg.id,
        channel: 'Instagram',
        notification_type: 'Direct Message',
        label: 'Negative',
        content: cnt,
        user: usr
    )
    # Serve these through API instead of messages
    redirect_to action: 'thread_messages_index', th_id: th
  end
=end

  def instagram_user_index
    @insta_users = InstagramUser.all
  end

  def insta_msg_nlp

    # TODO: Sentiment background job here
    # TODO: Rossete init should go in config

    msgs = InstagramMessage.all
    rosette_api = RosetteAPI.new(ROSETTE_API_KEY)

    msgs.each do |m|
      unless m.text.nil?
        puts m.text
        if m.sentiment.nil?
          file = Tempfile.new(%w(edify-msg-nlp))
          sentiment_file_data =  "what the fucl" # m.text
          file.write(sentiment_file_data)
          file.close
          params = DocumentParameters.new(file_path: file.path, language: 'eng')
          response = rosette_api.get_sentiment(params)
          m.sentiment = response['document']['label']
          m.save
        end
      end
    end

    # BG job for notifications

    respond_to do |format|
      format.json {
        render json: {
            msg: 'success'
        }
      }
    end
  end

  def instagram_messages_index
    @insta_messages = InstagramMessage.all.order('timestamp desc')
  end

  def instagram_user_show
    @insta_user = InstagramUser.find(params[:iuid])
  end

  def recipients_index
    #@recip_users = InstagramThread.find(params[:th_id]).recipients
    @recip_users = InstagramThread.find(params[:th_id]).instagram_users
  end

  def thread_messages_index
    @insta_msgs = InstagramThread.find(params[:th_id]).instagram_messages.order('timestamp desc')
    @child = Child.find(params[:id])
  end

  def mobile_apps
    @chmonctrl = ChildMonitorSetting.last

    sync_update = params[:sync_time]
    user = params[:user]
    if sync_update && sync_update != ''
      cm = ChildMonitorSetting.last
      cm.sync_time = sync_update
      cm.user = user
      cm.save
    end

  end

  def background_jobs
  end

  def api_editor
  end

  def api_docs
  end

  def language_model
    r = Monkeylearn.classifiers.detail('cl_MVmnhmeK')
    @class_details_eng = r.result['result']['classifier']
    @class_cats_eng = r.result['result']['sandbox_categories']

    rs = Monkeylearn.classifiers.detail('cl_r5iLWWFZ')
    @class_details_sp = rs.result['result']['classifier']
    @class_cats_sp = rs.result['result']['sandbox_categories']
  end

  def log_monitoring

  end

end
