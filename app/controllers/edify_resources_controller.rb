class EdifyResourcesController < ApplicationController
  before_action :set_edify_resource, only: [:show, :edit, :update, :destroy]

  # GET /edify_resources
  # GET /edify_resources.json
  def index
    @edify_resources = EdifyResource.all
  end

  # GET /edify_resources/1
  # GET /edify_resources/1.json
  def show
  end

  # GET /edify_resources/new
  def new
    @edify_resource = EdifyResource.new
  end

  # GET /edify_resources/1/edit
  def edit
  end

  # POST /edify_resources
  # POST /edify_resources.json
  def create
    @edify_resource = EdifyResource.new(edify_resource_params)

    respond_to do |format|
      if @edify_resource.save
        format.html { redirect_to @edify_resource, notice: 'Edify resource was successfully created.' }
        format.json { render :show, status: :created, location: @edify_resource }
      else
        format.html { render :new }
        format.json { render json: @edify_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /edify_resources/1
  # PATCH/PUT /edify_resources/1.json
  def update
    respond_to do |format|
      if @edify_resource.update(edify_resource_params)
        format.html { redirect_to @edify_resource, notice: 'Edify resource was successfully updated.' }
        format.json { render :show, status: :ok, location: @edify_resource }
      else
        format.html { render :edit }
        format.json { render json: @edify_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /edify_resources/1
  # DELETE /edify_resources/1.json
  def destroy
    @edify_resource.destroy
    respond_to do |format|
      format.html { redirect_to edify_resources_url, notice: 'Edify resource was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_edify_resource
      @edify_resource = EdifyResource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def edify_resource_params
      params.require(:edify_resource).permit(:link, :title, :site, :logo, :image, :brief, :tag_list)
    end
end
