module API
  class ChildMonitorSettingsController < API::BaseController

    skip_before_filter  :verify_authenticity_token,
                        :if => Proc.new { |c| c.request.format == 'application/json' }

    def sync_time
      sync_time = ChildMonitorSetting.last.sync_time
      render :status => :ok,
             :json => {
                 success: true,
                 info: 'Sync time in seconds',
                 data: {
                     sync_time: sync_time
                 }
             }
    end

  end
end