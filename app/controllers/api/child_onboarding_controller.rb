module API
  class ChildOnboardingController < API::BaseController
    skip_before_filter  :verify_authenticity_token,
                        if: proc { |c| c.request.format == 'application/json' }

    def activate_android
      pin = params[:pin]
      imei = params[:imei]
      # Look for child account
      ch = Child.find_by_pin(pin)
      if ch
        if imei
          fimei = imei.to_f
          ch.imei = imei if fimei > 0
        end
        ch.save
        user = ch.user
        status = :ok
        success = true
        info = 'Android OK',
               data = {
                 name: ch.name,
                 id: ch.id,
                 phone: ch.phone,
                 pin: ch.pin,
                 edify_user_name: user.name
               }
      else
        status = '200'
        success = false
        info = 'Could not activate'
        data = {}
      end
      render status: status,
             json: {
               success: success,
               info: info,
               data: { msg: data }
             }
    end

    def activate_ios
      pin = params[:pin]
      render status: 200,
             json: { success: true,
                     info: 'iOS OK',
                     data: { msg: pin } }
    end

    def add_child_account
      ch              = Child.new
      ch.name         = params[:name]
      ch.phone        = params[:child_phone]
      ch.instagram[0] = params[:instagram_user]
      ch.instagram[1] = params[:instagram_password]
      ch.image        = params[:base64_avatar]
      user = User.find(params[:edify_user])
      ch.user = user
      if ch.save

        if ch.account_sheets.first
          status = :ok
          success = true
          info = 'Created successfully.'
          data = {
            name:             ch.name,
            id:               ch.id,
            pin:              ch.pin,
            phone:            ch.phone,
            edify_user_name:  user.name
          }
        else
          status = :ok
          success = true
          info = 'Created successfully, provisioning in process.'
          data = {
            name:             ch.name,
            id:               ch.id,
            pin:              ch.pin,
            phone:            ch.phone,
            image:            ch.image.url,
            edify_user_name:  user.name
          }
        end
      else
        status = '500'
        success = false
        info = 'Unprocessable entity'
        data = {}
      end
      render  status: status,
              json: {
                success: success,
                info: info,
                data: data
              }
    end

    def remove_child_account
      ch = Child.find(params[:edify_id])
      if ch.account_sheets.count > 0
        acct = AccountSheet.find(ch.account_sheets.first.id)
        acct.status = 'removed'
        acct.pin = ''
        acct.instagram_account = []
        acct.child_id = nil
        acct.save
      end
      if ch.destroy
        status = :ok
        success = true
        info = 'Destroyed successfully'
      else
        status = '500'
        success = false
        info = 'Unprocessable entity'
      end
      data = {}
      render  status: status,
              json: {
                success: success,
                info: info,
                data: data
              }
    end

    def add_avatar
      @child = Child.find(params[:id])
      @child.image = params[:base64_avatar]
      if @child.save
        render status: 200,
               json: {
                 success: true,
                 info: 'New image saved',
                 data: {
                   image_url: @child.image
                 }
               }
      else
        render status: :unprocessable_entity,
               json: {
                 success: false,
                 info: 'Error image was not uploaded',
                 data: {}
               }
      end
    end

    def update_instagram
      child = Child.find(params[:id])
      new_insta_usrn = params[:instagram_username]
      new_insta_pswd = params[:instagram_password]

      child.instagram[1] = new_insta_pswd if new_insta_pswd

      child.instagram[0] = new_insta_usrn if new_insta_usrn

      if child.save!
        render status: 200,
               json: {
                 success: true,
                 info: 'Instagram credentials updated.',
                 data: {
                   instagram_username: child.instagram[0],
                   instagram_password: child.instagram[1]
                 }
               }
      else
        render status: :unprocessable_entity,
               json: {
                 success: false,
                 info: 'Error update not available',
                 data: {}
               }
      end
    end

    def update_child
      child = Child.find(params[:id])
      child.name  = params[:child_name]
      child.phone = params[:child_phone]
      if child.save!
        render status: 200,
               json: {
                 success: true,
                 info: 'Child profile updated.',
                 data: {
                   new_name: child.name,
                   new_phone: child.phone
                 }
               }
      else
        render status: :unprocessable_entity,
               json: {
                 success: false,
                 info: 'Error update not available',
                 data: {}
               }
      end
    end
  end
end
