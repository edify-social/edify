module API
  class InstaDataController < API::BaseController

    skip_before_filter  :verify_authenticity_token,
                        :if => Proc.new { |c| c.request.format == 'application/json' }

    before_action :set_insta_datum, only: [:show, :edit, :update, :destroy]

    def main_feed
      # TODO: Make this a background job
      i_pin = params[:pin]

      # thread = Thread.find_by_thread_id()
      entries = params[:entries]
      th_ids = []
      child = Child.find_by_pin(i_pin)

      if child
        # Sub accounts exists
        entries.each do |e|
          # Each entry is a thread
          inst_thread_id = e[:thread_id]
          th_ids << inst_thread_id
          inst_thread = InstagramThread.find_by_thread_id(inst_thread_id)
          inst_th_new = 'False'

          # New thread
          unless inst_thread
            inst_thread = InstagramThread.new
            inst_th_new = 'True'
            inst_thread.children << child
          end

          # Set thread info
          inst_thread.thread_id     = e[:thread_id]
          inst_thread.thread_title  = e[:thread_title]
          inst_thread.last_activity = e[:last_activity_at]
          inst_thread.muted         = e[:muted]
          inst_thread.named         = e[:named]
          inst_thread.canonical     = e[:canonical]

          # Timestamp converter
          stmp = inst_thread.last_activity.to_i / 1000000
          tstmp = Time.at(stmp)

          # If new data then save. New information has been uploaded.
          if inst_th_new == 'True'
            if inst_thread.save
              all_messages = e[:cached_messages]
              all_messages.each do |msd|
                create_message(msd, inst_thread)
              end
            end
          else
            if inst_thread.updated_at > tstmp && inst_thread.save
              all_messages = e[:cached_messages]
              all_messages.each do |msd|
                create_message(msd, inst_thread)
              end
            end
          end

        end
        # Entries iteration ended
        req_status = 200
        req_success = 'True'
        info = 'Entries iteration complete'
        data = {
            msg: 'All threads reviewed'
        }
      else
        # Sub account doesn't exist
        req_status = 200
        req_success = 'False'
        info = 'No sub account with the requested pin found'
        data = {
           msg: 'PIN not valid: ' + i_pin
        }
      end

      render status: req_status,
             json: {
                 success: req_success,
                 info: info,
                 data: data
             }
    end

    # GET /insta_data
    # GET /insta_data.json
    def index
      @insta_data = InstaDatum.all
    end

    # GET /insta_data/1
    # GET /insta_data/1.json
    def show

    end

    # GET /insta_data/new
    def new
      @insta_datum = InstaDatum.new
    end

    # GET /insta_data/1/edit
    def edit
    end

    # POST /insta_data
    # POST /insta_data.json
    def create
      @insta_datum = InstaDatum.new(insta_datum_params)

      respond_to do |format|
        if @insta_datum.save
          format.html { redirect_to @insta_datum, notice: 'Insta datum was successfully created.' }
          format.json { render :show, status: :created, location: @insta_datum }
        else
          format.html { render :new }
          format.json { render json: @insta_datum.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /insta_data/1
    # PATCH/PUT /insta_data/1.json
    def update
      respond_to do |format|
        if @insta_datum.update(insta_datum_params)
          #format.html { redirect_to @insta_datum, notice: 'Insta datum was successfully updated.' }
          format.json { render :show, status: :ok, location: @insta_datum }
        else
          #format.html { render :edit }
          format.json { render json: @insta_datum.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /insta_data/1
    # DELETE /insta_data/1.json
    def destroy
      @insta_datum.destroy
      respond_to do |format|
        format.html { redirect_to insta_data_url, notice: 'Insta datum was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_insta_datum
      @insta_datum = InstaDatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def insta_datum_params
      params.require(:insta_datum).permit( {content: []})
    end

    def new_instagram_msg_user(msg_user)
      new_iuser = InstagramUser.new
      new_iuser.username = msg_user[:user][:username]
      new_iuser.is_staff = msg_user[:user][:is_staff]
      new_iuser.coeff_weight = msg_user[:user][:coeff_weight]
      new_iuser.usertag_review_enabled = msg_user[:user][:usertag_review_enabled]
      new_iuser.profile_pic_url = msg_user[:user][:profile_pic_url]
      new_iuser.full_name = msg_user[:user][:full_name]
      new_iuser.is_verified = msg_user[:user][:is_verified]
      new_iuser.uid = msg_user[:user][:id]
      new_iuser.is_private = msg_user[:user][:is_private]

      # New not included
      # is_geo_ip_blocked
      # has_anonymous_profile_picture
      # is_new

      # Not used:
      # - Can be old and ignored (deprecate?)
      # - Not used becausee they are not icnluded in msg users but could be used
      #   somewhere else in the file
      # new_iuser.external_url = msg_user[:user][:external_url]
      # new_iuser.follower_count = msg_user[:user][:follower_count]
      # new_iuser.following_count = msg_user[:user][:following_count]
      # new_iuser.media_count = msg_user[:user][:media_count]
      # new_iuser.geo_media_count = msg_user[:user][:geo_media_count]
      # new_iuser.usertags_count = msg_user[:user][:usertags_count]
      # new_iuser.byline = msg_user[:user][:byline]
      # new_iuser.chaining_suggestions = msg_user[:user][:chaining_suggestions]
      # new_iuser.can_see_organic_insights = msg_user[:user][:can_see_organic_insights]

      new_iuser.save
      new_iuser
    end

    def create_message(msg, thd)
      insta_msg = InstagramMessage.find_by_item_id(msg[:item_id])
      new_msg = 'False'

      unless insta_msg
        insta_msg = InstagramMessage.new
        new_msg = 'True'
      end

      # Generic message fields
      insta_msg.content_type        = msg[:content_type]
      insta_msg.status              = msg[:status]
      insta_msg.item_type           = msg[:item_type]
      insta_msg.item_id             = msg[:item_id]
      insta_msg.timestamp           = msg[:timestamp]
      insta_msg.timestamp_in_micro  = msg[:timestamp_in_micro]
      insta_msg.uid                 = msg[:user_id]
      insta_msg.thread_key[0]       = msg[:thread_key][:thread_id]
      insta_msg.thread_key[1]       = msg[:thread_key][:recipient_ids]
      insta_msg.hide_in_thread      = msg[:hide_in_thread]

      # If message is text
      if insta_msg.content_type == 'TEXT'
        insta_msg.text              = msg[:text]
      end

      # Get msg user
      msg_user = InstagramUser.find_by_uid(msg[:user][:id])
      insta_msg.save
      
      unless msg_user
        msg_user = new_instagram_msg_user(msg)
      end

      # Store users
      insta_msg.instagram_users << msg_user

      # If message is action log
      if insta_msg.content_type == 'ACTION_LOG'
        insta_msg.action_log        = msg[:action_log][:description]
      end

      # TODO if message is media_share
      # TODO if message is like
      # Specific fields not currently used
      # new_insta_message.client_context = incoming[:cached_messages][idx][:client_context]
      # new_insta_message.preview_medias = incoming[:cached_messages][idx][:preview_medias]

      # If msg is new associate with thread
      if new_msg == 'True'
        thd.instagram_messages << insta_msg
      end

      # Save all changes
      insta_msg.save

    end

  end
end
