module API
  class AccountSheetsController < API::BaseController

    skip_before_filter  :verify_authenticity_token,
                        :if => Proc.new { |c| c.request.format == 'application/json' }

    def emulator_accounts
      @hostname = params[:hostname]
      api_action = params[:api_action]
      first_run = params[:first_run]
      people = params[:people]
      node = Node.find_by_hostname(@hostname)
      if node
        @acct_sheets = AccountSheet.where(server: @hostname).order(:id)
        if api_action == 'update'
          @acct_sheets.each_with_index do |a, i|
            a.status      = people[i]['status']
            a.last_run      = people[i]['last_run']
            if people[i]['info']
              a.emulator      = people[i]['info'][0]
              a.health_check  = people[i]['info'][1]
            end
            a.save
          end
        elsif api_action == 'sleep'
          region = node.region
          inst_id = node.aws_id
          ec2 = Aws::EC2::Resource.new(region: region)
          i = ec2.instance(inst_id )
          inst = inst_id
          if i.exists?
            case i.state.code
              when 48  # terminated
                puts "#{inst} is terminated, can't be stopped"
              when 64  # stopping
                puts "#{inst} is stopping, please wait."
              when 89  # stopped
                puts "#{inst} is already stopped"
              else
                i.stop
            end
          end
          InfraUpdateWorker.perform_async
          if first_run
            timestamp = Time.at(first_run.to_i)
            node.wake_up = timestamp + 12.hours
          else
            node.wake_up = Time.now + 12.hours
          end
          node.save
        end

      else
        status = '500'
        success = false
        info = "D'oh!"
        data = {}
        render status: status,
               json: {
                   success: success,
                   info: info,
                   data: { msg: data }
               }
      end
    end

  end
end
