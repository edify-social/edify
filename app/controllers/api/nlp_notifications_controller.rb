module API
  class NlpNotificationsController < API::BaseController
    skip_before_filter  :verify_authenticity_token,
                        if: proc { |c| c.request.format == 'application/json' }

    def child_notifications
      child_pin = params[:pin]
      child = Child.find_by_pin(child_pin)

      if child
        @notifs = child.edify_notifications.where(is_active: true)

        if @notifs.length <= 0
          status = '200'
          success = true
          info = 'No notifications yet'
          data = {
            notifications: 'none'
          }
          render status: status,
                 json: {
                   success: success,
                   info: info,
                   data: { msg: data }
                 }
        end
      end
    end

    def update_notification
      # Send feedback here from mobile

      child_pin = params[:pin]
      child = Child.find_by_pin(child_pin)
      notifs = params[:notifications]
      notif_type = params[:inactive_warning]

      if notif_type
        id = params[:id]
        stored_notif = EdifyNotification.find(id)
        stored_notif.destroy
      else
        notifs.each do |n|
          stored_notif = DeviceMessage.find(n['id']).edify_notifications.first
          stored_notif.status = n['status']
          stored_notif.feedback = n['feedback']
          stored_notif.is_active = n['is_active']
          stored_notif.save
        end
      end

      if child.save
        status = '200'
        success = true
        info = 'Update received'
        data = {
          notifications: notifs
        }
      else
        status = '500'
        success = false
        info = 'Update error'
        data = {}
      end

      render status: status,
             json: {
               success: success,
               info: info,
               data: { msg: data }
             }
    end
  end
end
