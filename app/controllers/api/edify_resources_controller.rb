module API
  class EdifyResourcesController < API::BaseController

    skip_before_filter  :verify_authenticity_token,
                        :if => Proc.new { |c| c.request.format == 'application/json' }

    def index
      @resources = EdifyResource.all
    end

  end
end