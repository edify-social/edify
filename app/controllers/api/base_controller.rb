module API
  class BaseController < ApplicationController
    protect_from_forgery with: :null_session
    skip_before_filter :authenticate_user!
    respond_to :json
  end
end