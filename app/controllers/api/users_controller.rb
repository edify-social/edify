module API
  class UsersController < API::BaseController

    skip_before_filter  :verify_authenticity_token,
                        :if => Proc.new { |c| c.request.format == 'application/json' }

    def index
      # TODO: Scope to non admins
      @users = User.all
    end

    def show
      @user = User.where(id: params[:id])
      if @user.length <= 0
        status = :unprocessable_entity
        success = false
        info = 'User not found'
        data = {}
        render  status: status,
                json: {
                    success: success,
                    info: info,
                    data: data
                }
      end
      @user = @user.first
    end

    def create
      email = params[:email]
      name = params[:name]
      password = params[:password]
      password_c = params[:password_confirmation]
      phone = params[:phone]
      u = User.new()
      u.email = email
      u.name = name
      u.password = password
      u.phone = phone
      if password == password_c
        if u.save
          status = :ok
          success = true
          info = 'User created successfully'
          data = {
              id: u.id,
              name: u.name,
              email: u.email,
              phone: u.phone,
              auth_token: u.authentication_token,
          }
        else
          status =  :unprocessable_entity
          success = false
          info = 'Verify data'
          data = []
        end
      else
        status =  :unprocessable_entity
        success = false
        info = 'Password mismatch'
        data = []
      end
      render  status: status,
              json: {
                  success: success,
                  info: info,
                  data: data
              }
    end

    def update
      # Check if email has been taken and passwords match
      if User.exists?(email: params[:email])
        status = :unprocessable_entity
        success = false
        info = params[:email] + ' this email has already been taken.',
        data = {}
      else
        # User exists
        @user = User.find(params[:id])

        unless params[:name].nil?
          if params[:name].length > 0
            @user.name = params[:name]
          end
        end
        unless params[:email].nil?
          if params[:email].length > 0
            @user.email = params[:email]
          end
        end
        unless params[:phone].nil?
          if params[:phone].length > 0
            @user.phone = params[:phone]
          end
        end

        if @user.save!
          status = :ok
          success = true
          info = 'Successful update.'
          data = {
              name: @user.name,
              email: @user.email,
              phone: @user.phone
          }
        else
          status = :unprocessable_entity
          success = false
          info = 'Something went wrong.'
          data = {}
        end
      end # Ends user update

      # Final Render
      render  status: status,
              json: {
                  success: success,
                  info: info,
                  data: data
              }
    end

    # ToDo: Deprecate update password and avatar.

    def update_password
      # TODO: Handle password chane through Devise
      # Min password length
      pwd_length = 8
      # Process Password
      if !params[:password].nil? && !params[:password_confirmation].nil?

        # Password and confirmation match
        if params[:password] == params[:password_confirmation]

          # Min password length
          if params[:password].length >= pwd_length
            @user.password = params[:password]
            @user.password_confirmation = params[:password_confirmation]
            @user.save!
            status = :ok
            success = true
            info = 'Successful update.'
            data = {
                name: @user.name,
                email: @user.email,
                phone: @user.phone
            }
          else
            status = :unprocessable_entity
            success = false
            info = 'Password should be at least ' + pwd_length.to_s + ' characters long.',
                data = {}
          end
        else
          status = :unprocessable_entity
          success = false
          info = 'Password and password confirmation don not match.',
          data = {}
        end

        # Final Render
        render  status: status,
                json: {
                    success: success,
                    info: info,
                    data: data
                }
      end # Ends password process
    end

    def add_avatar
      @user = User.find(params[:id])
      @user.avatar = params[:base64_avatar]
      if @user.save!
        render :status => 200,
               :json => { :success => true,
                          :info => 'new image',
                          :data => {}
               }
      else
        render :status => :unprocessable_entity,
               :json => { :success => false,
                          :info => 'Error',
                          :data => {} }
      end
    end

    def register_for_push
      @user = User.find(params[:id])

      @user.player_id = params[:player_id]
      @user.push_token = params[:push_token]

      if @user.save!
        status = 200
        success = true
        info = 'Registered'
        data =''
      else
        status = 200
        success = false
        info = 'Something went wrong on our end.'
        data = ''
      end

      render status: status,
             json: {
               success: success,
               info: info,
               data: data
             }
    end

    def purchase_backtrack
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        @user = User.find(child.user.id)
        if @user
          @user.backtrack = true
          if @user.save!
            status = 200
            success = true
            info = 'Purchased backtrack'
            data =''
          else
            status = 200
            success = false
            info = 'Purchase failed'
            data = 'Error saving record'
          end
        else
          status = 200
          success = false
          info = 'Purchase failed'
          data = 'User not found'
        end
      else
        status = 200
        success = false
        info = 'Purchase failed'
        data = 'Child not found'
      end
      render status: status,
             json: {
                 success: success,
                 info: info,
                 data: data
             }
    end

    def check_device_connection
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        status = 200
        success = true
        info = 'Check received'
        data = {
            connected: child.device_connected
        }
      else
        status = 200
        success = false
        info = 'Check failed'
        data = 'Child not found'
      end
      render status: status,
             json: {
                 success: success,
                 info: info,
                 data: data
             }
    end

  end
end