module API
  class ChildMobileDataController < API::BaseController

    skip_before_filter  :verify_authenticity_token,
                        :if => Proc.new { |c| c.request.format == 'application/json' }


    def upload_messages
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        messages = params[:messages]
        update_checkin(pin)
        if messages
          messages.each do |m|
            nm = DeviceMessage.new
            nm.message_id = m[:_id]
            nm.message = m[:body]
            nm.child = child
            raw_time = m[:date]
            nm.mobile_timestamp = Time.at(raw_time.to_i/1000)
            nm.contact_name = m[:contact_name]

            m[:type] == 2 || m[:type] == "2" ? nm.sent = true : nm.sent = false

            nm.thread_id = m[:thread_id]
            nm.contact_number = m[:address]
            nm.save
          end
          status = 200
          success = true
          info = 'All saved'
          data = child.id
        else
          status = 200
          success = true
          info = 'No new messages received'
          data = child.id
        end
      else
        status = 500
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
               success: success,
               info: info,
               data: data
             }
    end

    def last_message_saved
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        if child.device_messages
          if !child.device_messages.empty?
            dm = child.device_messages.last

            status = 200
            success = true
            info = 'Child found, sending last message'
            data = {
              message_id: dm.id,
              # contact_name: dm.contact_name,
              contact_phone: dm.contact_number,
              content: dm.message,
              created_on_server: dm.created_at
            }
          else
            status = 200
            success = false
            info = 'No child or messages found'
            data = ''
          end
        else
          status = 200
          success = false
          info = 'No child or messages found'
          data = ''
        end
      else
        status = 200
        success = false
        info = 'No child or messages found'
        data = ''
      end

      render status: status,
             json: {
               success: success,
               info: info,
               data: data
             }
    end

    def upload_app_list
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        app_list = params[:applications]
        prev_lists = child.app_list
        overwrite = false

        # Sets application list
        if prev_lists.count == 0
          # First AppList
          apl = AppList.new
          # Stores applications from params into attribute
          app_list.each do |al|
            apl.applications << [al, 'new']
          end
        else
          # Second AppList
          apl = AppList.new
          overwrite = true
          compare_list = prev_lists.first.applications.map { |x| x[0] }

          # Stores applications from params into attribute
          app_list.each do |al|
            compare_list.include?(al) ?
                apl.applications << [al, 'old'] :
                apl.applications << [al, 'new']
          end
        end

        # Gets new applications for notification
        overwrite ?
            new_apps = check_for_new_apps(apl, child) :
            new_apps = apl.applications.map {|x| x[0]}

        unless new_apps.empty?
          cnt = new_apps.join(', ')

          # Generates new notification
          notify child, cnt
        end

        # Save app list
        if overwrite
          prev_list = AppList.find(child.app_list.last.id)
          prev_list.applications = apl.applications
          prev_list.save
        else
          apl.child_id = child.id
          child.app_list << apl
        end

        # Report app has connected
        update_checkin(pin)

        # Save changes
        if child.save
          puts 'saved!'
        else
          puts 'error'
        end

        status = 200
        success = true
        info = 'All saved'
        data = {
          child_id: child.id,
          new_apps: new_apps
        }

      else
        status = 200
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
               success: success,
               info: info,
               data: data
             }
    end

    def check_for_new_apps(data, ch)
      prev_apps = ch.app_list.last(2).first.applications.map { |x| x[0] }
      current_apps = data.applications.map { |x| x[0] }
      delta = []

      current_apps.each do |pa|
        delta << pa unless prev_apps.include?(pa)
      end

      delta
    end

    def notify(chld, content)

      # Push notification
      params = {
          app_id: '8c010546-76ff-40f6-8761-5163eb01f8dd',
          contents:
              {
                  en: "New apps installed on device: #{content}"
              },
          ios_badgeType: 'Increase',
          ios_badgeCount: 1,
          include_player_ids: [chld.user.player_id]
      }
      uri = URI.parse('https://onesignal.com/api/v1/notifications')
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      request = Net::HTTP::Post.new(uri.path,
                                    'Content-Type'  => 'application/json;charset=utf-8',
                                    'Authorization' => 'Basic ZGM4MDUxMjgtMzZlMi00MjJkLWE4NjgtZTNjYTc2YjVjMTc4')
      request.body = params.as_json.to_json
      http.request(request)

      channel = 'App List'
      label = 'Info'
      contnt = "New apps installed on device: #{content}"

      # Edify Notification
      notification(chld.id, channel, label, contnt, chld.user)
    end

    def app_list
      id = params[:id]
      child = Child.find(id)

      if child

        al = child.app_list.last

        status = 200
        success = true
        info = 'App list'
        data = al

      else
        status = 200
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
               success: success,
               info: info,
               data: data
             }
    end

    def device_messages
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child

        dm = child.device_messages

        status = 200
        success = true
        info = 'Device messages'
        data = dm

      else
        status = 500
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
               success: success,
               info: info,
               data: data
             }
    end

    def backtrack_check
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child

        dm = child.user

        status = 200
        success = true
        info = 'User found sending Backtrack state'
        data = {
          backtrack: dm.backtrack?,
          status: child.backtrack_status
        }

      else
        status = 200
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
                 success: success,
                 info: info,
                 data: data
             }
    end

    def backtrack
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        messages = params[:messages]
        messages.each do |m|
          nm = DeviceMessage.find_by_message_id(m[:_id])
          if nm
            unless nm.backtrack?
              nm = DeviceMessage.new
              nm.message_id = m[:_id]
              nm.message = m[:body]
              nm.child = child
              raw_time = m[:date]
              nm.mobile_timestamp = Time.at(raw_time.to_i/1000)
              nm.contact_name = m[:contact_name]

              m[:type] == 2 || m[:type] == "2" ? nm.sent = true : nm.sent = false

              nm.thread_id = m[:thread_id]
              nm.contact_number = m[:address]
              nm.backtrack = true
              nm.save
            end
          else
            nm = DeviceMessage.new
            nm.message_id = m[:_id]
            nm.message = m[:body]
            nm.child = child
            raw_time = m[:date]
            nm.mobile_timestamp = Time.at(raw_time.to_i/1000)
            nm.contact_name = m[:contact_name]

            m[:type] == 2 || m[:type] == "2" ? nm.sent = true : nm.sent = false

            nm.thread_id = m[:thread_id]
            nm.contact_number = m[:address]
            nm.backtrack = true
            nm.save
          end
        end

        child.backtrack_status = 1
        child.save

        status = 200
        success = true
        info = 'All saved'
        data = child.id
      else
        status = 500
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
                 success: success,
                 info: info,
                 data: data
             }
    end

    def request_backtrack
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        child.backtrack_status = 0
        child.save

        status = 200
        success = true
        info = 'Backtrack requested'
        data = child.backtrack_status
      else
        status = 500
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
                 success: success,
                 info: info,
                 data: data
             }
    end

    def upload_contacts
      pin = params[:pin]
      child = Child.find_by_pin(pin)

      if child
        Contact.create(content: params[:contacts], child_id: child.id)

        status = 200
        success = true
        info = 'Contacts uploaded'
        data = child.backtrack_status
      else
        status = 200
        success = false
        info = 'No child found'
        data = ''
      end

      render status: status,
             json: {
                 success: success,
                 info: info,
                 data: data
             }
    end

    private

    def update_checkin(pin)
      child = Child.find_by_pin(pin)
      return false unless child

      child.last_device_checkin = DateTime.now
      child.device_connected = true
      child.save
    end

    def notification(ch_id, channel, label, content, usr)
      EdifyNotification.create(
          child_id: ch_id,
          channel: channel,
          device_message_id: 0,
          notification_type: 'Device',
          label: label,
          content: content,
          user: usr
      )
    end

  end
end
