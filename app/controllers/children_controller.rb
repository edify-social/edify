class ChildrenController < ApplicationController
  before_action :set_child, only: [:show, :edit, :update, :destroy,
                                   :notifications, :device_messages,
                                   :device_messages_table, :device_message_thread,
                                   :backtracked_table, :backtracked_thread]

  # GET /children
  # GET /children.json
  def index
    @children = Child.all
  end

  # GET /children/1
  # GET /children/1.json
  def show
  end

  # GET /children/new
  def new
    @child = Child.new
  end

  # GET /children/1/edit
  def edit
  end

  # POST /children
  # POST /children.json
  def create
    @child = Child.new(child_params)

    respond_to do |format|
      if @child.save
        format.html { redirect_to @child, notice: 'Child was successfully created.' }
        format.json { render :show, status: :created, location: @child }
      else
        format.html { render :new }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /children/1
  # PATCH/PUT /children/1.json
  def update
    respond_to do |format|
      if @child.update(child_params)
        format.html { redirect_to @child, notice: 'Child was successfully updated.' }
        format.json { render :show, status: :ok, location: @child }
      else
        format.html { render :edit }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /children/1
  # DELETE /children/1.json
  def destroy
    @child.destroy
    respond_to do |format|
      format.html { redirect_to children_path, notice: 'Child was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def remove_child_thread
    ch_thread = InstagramThread.find(params[:id])
    ch_thread.destroy
    respond_to do |format|
      format.html { redirect_to children_path, notice: 'Thread was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def remove_device_message
    ch_dm = DeviceMessage.find(params[:id])
    ch_dm.destroy
    respond_to do |format|
      format.html { redirect_to children_path, notice: 'Device Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def remove_notification
    ch_notif = EdifyNotification.find(params[:id])
    ch_notif.destroy
    respond_to do |format|
      format.html { redirect_to children_path, notice: 'Notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Notification: edify_notification + context
  # Context: Messages related to the alert. Alerted msg + any prior and post
  # msg during within a week.
  def notifications
    # This is set for device push but we don't need it to visualize notifs here
    # due to admin having use of all not only active notifs
    # notifs = @child.edify_notifications.where(is_active: true)

    notifs = @child.edify_notifications
    @notifications = []
    warnings = []
    notifs.each do |n|
      if n.device_message_id > 0
        msg_context = n.child.device_messages.where(thread_id: n.device_message.thread_id).where(mobile_timestamp:  (Time.parse(n.device_message.mobile_timestamp) - 7.days)..(Time.parse(n.device_message.mobile_timestamp) + 7.days)).limit(200)
        @notifications << msg_context
      else
        warnings << n
      end
    end
    @warnings = Kaminari.paginate_array(warnings).page(params[:page]).per(10)
    @notifications
  end

  def device_messages
    @device_messages = @child.device_messages.where('device_messages.backtrack IS NOT true').order(mobile_timestamp: :desc)
  end

  def device_messages_table
    if params[:search]
      @device_messages = @child.device_messages.where('device_messages.backtrack IS NOT true').search(params[:search]).order("created_at DESC").page(params[:page]).per(25)
    else
      @device_messages = @child.device_messages.where('device_messages.backtrack IS NOT true').page(params[:page]).per(25)
    end
  end

  def device_message_thread
    thread = params[:thread_id]
    @device_messages = @child.device_messages.where('device_messages.backtrack IS NOT true').where(thread_id: thread).order(mobile_timestamp: :desc)
  end

  def backtracked_table
    if params[:search]
      @device_messages = @child.device_messages.where(backtrack: true).search(params[:search]).order("created_at DESC").page(params[:page]).per(25)
    else
      @device_messages = @child.device_messages.where(backtrack: true).page(params[:page]).per(25)
    end
  end

  def backtracked_thread
    thread = params[:thread_id]
    @device_messages = @child.device_messages.where(thread_id: thread).order(mobile_timestamp: :desc)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_child
      @child = Child.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def child_params
      params.require(:child).permit(:name, :phone, :imei, :user_id, :backtrack_status)
    end
end
