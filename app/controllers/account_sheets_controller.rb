class AccountSheetsController < ApplicationController
  before_action :set_account_sheet, only: [:show, :edit, :update]

  # GET /account_sheets/1
  # GET /account_sheets/1.json
  def show
  end

  # GET /account_sheets/1/edit
  def edit
    @status_list = ['none', 'taken', 'assigned', 'removed', 'running', 'unknown']
  end

  # PATCH/PUT /account_sheets/1
  # PATCH/PUT /account_sheets/1.json
  def update
    respond_to do |format|
      if @account_sheet.update(account_sheet_params)
        @account_sheet.instagram_account[0] = params[:account_sheet][:instagram_account][:username]
        @account_sheet.instagram_account[1] = params[:account_sheet][:instagram_account][:password]
        @account_sheet.save
        format.html { redirect_to @account_sheet, notice: 'Account sheet was successfully updated.' }
        format.json { render :show, status: :ok, location: @account_sheet }
      else
        format.html { render :edit }
        format.json { render json: @account_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account_sheet
      @account_sheet = AccountSheet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_sheet_params
      params.require(:account_sheet).permit(:pin, :child_id, :status, :server, :emulator, :node_id, :health_check)
    end
end
