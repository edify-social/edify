class InfrastructureController < ApplicationController
  before_action :set_node, only: [:show, :destroy]

  DEBUG_INDEX = false

  def index
    @debug = DEBUG_INDEX
    @nodes = Node.all
    @aws_regions = AwsRegion.where(active: true)
  end

  def show
    node = Node.find(params[:id])
    if node.aws_id == 'i-eaded85f'
      @staging = Node.find_by_aws_id('i-eaded85f')
    else
      @staging = nil
    end
  end

  def terminate_node
    aws_inst = params[:inst_id]
    TerminateInstanceWorker.perform_async(aws_inst)

    redirect_to(infrastructure_index_path, notice: 'Instance termination scheduled.')
  end

  def provision_node
    # Provision with AWS
    ProvisionInstanceWorker.perform_async(0)

    if Rails.env.development?
      redirect_to(infrastructure_index_path,
                  notice: 'Instance provioning scheduled. Launched at us-east-2, make sure you have it in your aws regions.'
      )
    else
      redirect_to(infrastructure_index_path,
                  notice: 'Instance provioning scheduled. Launched at us-west-2, make sure you have it in your aws regions.'
      )
    end
  end

  def destroy
    @node.destroy
    respond_to do |format|
      format.html { redirect_to infrastructure_index_path, notice: 'Node record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def infra_sync
    @job_id = InfraUpdateWorker.perform_async
  end

  def percentage_done
    job_id = params[:job_id]
    # Using the sidekiq_status gem to query the background job for progress information.
    container = SidekiqStatus::Container.load(job_id)
    @pct_complete = container.pct_complete

    respond_to do |format|
      format.json {
        render json: {
            percentage_done: @pct_complete
        }
      }
    end
  end

  def stopnode

    ec2 = Aws::EC2::Resource.new(region: params[:region])

    i = ec2.instance(params[:inst_id])

    inst = params[:inst_id]

    if i.exists?
      case i.state.code
        when 48  # terminated
          puts "#{inst} is terminated, can't be stopped"
        when 64  # stopping
          puts "#{inst} is stopping, please wait."
        when 89  # stopped
          puts "#{inst} is already stopped"
        else
          i.stop
      end
    end

    InfraUpdateWorker.perform_async

    redirect_to(infrastructure_index_path)
  end

  def startnode

    ec2 = Aws::EC2::Resource.new(region: params[:region])

    i = ec2.instance(params[:inst_id])

    inst = params[:inst_id]

    if i.exists?
      case i.state.code
        when 0  # pending
          puts "#{inst} is pending, so it will be running in a bit"
        when 16  # started
          puts "#{inst} is already started"
        when 48  # terminated
          puts "#{inst} is terminated, so you cannot start it"
        else
          i.start
      end
    end

    InfraUpdateWorker.perform_async

    redirect_to(infrastructure_index_path)
  end

  private

  def set_node
    @node = Node.find(params[:id])
  end

end
