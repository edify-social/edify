class EdifyNotificationsController < ApplicationController
  before_action :set_edify_notification, only: [:show, :edit, :update, :destroy]

  # GET /edify_notifications
  # GET /edify_notifications.json
  def index
    @edify_notifications = EdifyNotification.all
  end

  # GET /edify_notifications/1
  # GET /edify_notifications/1.json
  def show
  end

  # GET /edify_notifications/new
  def new
    @edify_notification = EdifyNotification.new
  end

  # GET /edify_notifications/1/edit
  def edit
  end

  # POST /edify_notifications
  # POST /edify_notifications.json
  def create
    @edify_notification = EdifyNotification.new(edify_notification_params)

    respond_to do |format|
      if @edify_notification.save
        format.html { redirect_to @edify_notification, notice: 'Edify notification was successfully created.' }
        format.json { render :show, status: :created, location: @edify_notification }
      else
        format.html { render :new }
        format.json { render json: @edify_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /edify_notifications/1
  # PATCH/PUT /edify_notifications/1.json
  def update
    respond_to do |format|
      if @edify_notification.update(edify_notification_params)
        format.html { redirect_to @edify_notification, notice: 'Edify notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @edify_notification }
      else
        format.html { render :edit }
        format.json { render json: @edify_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /edify_notifications/1
  # DELETE /edify_notifications/1.json
  # edify_notifications_url
  def destroy
    @edify_notification.destroy
    respond_to do |format|
      format.html { redirect_to children_path, notice: 'Edify notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_edify_notification
      @edify_notification = EdifyNotification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def edify_notification_params
      params.require(:edify_notification).permit(:msg_id, :content, :user, :label, :status, :type, :channel, :feedback)
    end
end
