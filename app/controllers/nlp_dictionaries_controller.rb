class NlpDictionariesController < ApplicationController
  before_action :set_nlp_dictionary, only: [:show, :edit, :update, :destroy]

  # GET /nlp_dictionaries
  # GET /nlp_dictionaries.json
  def index
    @nlp_dictionaries = NlpDictionary.all
  end

  # GET /nlp_dictionaries/1
  # GET /nlp_dictionaries/1.json
  def show
  end

  # GET /nlp_dictionaries/new
  def new
    @nlp_dictionary = NlpDictionary.new
  end

  def new_additional_meaning
    @nlp_dictionary = NlpDictionary.new
  end

  # GET /nlp_dictionaries/1/edit
  def edit
  end

  def index_add_mean
    @nlp_dictionaries = NlpDictionary.where('weight < ?', 100)
  end

  # POST /nlp_dictionaries
  # POST /nlp_dictionaries.json
  def create
    @nlp_dictionary = NlpDictionary.new(nlp_dictionary_params)

    respond_to do |format|
      if @nlp_dictionary.save
        format.html { redirect_to @nlp_dictionary, notice: 'Nlp dictionary was successfully created.' }
        format.json { render :show, status: :created, location: @nlp_dictionary }
      else
        format.html { render :new }
        format.json { render json: @nlp_dictionary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nlp_dictionaries/1
  # PATCH/PUT /nlp_dictionaries/1.json
  def update
    respond_to do |format|
      if @nlp_dictionary.update(nlp_dictionary_params)
        format.html { redirect_to @nlp_dictionary, notice: 'Nlp dictionary was successfully updated.' }
        format.json { render :show, status: :ok, location: @nlp_dictionary }
      else
        format.html { render :edit }
        format.json { render json: @nlp_dictionary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nlp_dictionaries/1
  # DELETE /nlp_dictionaries/1.json
  def destroy
    @nlp_dictionary.destroy
    respond_to do |format|
      format.html { redirect_to nlp_dictionaries_url, notice: 'Nlp dictionary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nlp_dictionary
      @nlp_dictionary = NlpDictionary.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nlp_dictionary_params
      params.require(:nlp_dictionary).permit(:content, :label, :weight)
    end
end
