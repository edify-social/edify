json.extract! nlp_dictionary, :id, :content, :label, :created_at, :updated_at
json.url nlp_dictionary_url(nlp_dictionary, format: :json)