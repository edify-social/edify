json.extract! aws_region, :id, :name, :code, :active, :created_at, :updated_at
json.url aws_region_url(aws_region, format: :json)