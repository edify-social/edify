json.user do
  json.id                     @user.id
  json.name                   @user.name
  json.email                  @user.email
  json.phone                  @user.phone
  json.backtrack              @user.backtrack
  json.edify_children @user.children do |child|
    json.child_id             child.id
    json.child_name           child.name
    json.phone                child.phone
    json.image                child.image
    json.pin                  child.pin
    json.backtrack_status     child.backtrack_status
    json.instagram_user       child.instagram[0]
  end
end
