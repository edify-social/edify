json.users @users do |user|
  json.id         user.id
  json.name       user.name
  json.email      user.email
  json.admin      user.admin
  json.token      user.authentication_token
end