json.server       @hostname
json.people @acct_sheets.order('id ASC') do |sheet|
  json.ash_id                   sheet.id
  json.status                   sheet.status
  json.last_run                 sheet.last_run
  json.pin                      sheet.pin
  json.instagram_account        [
                                    sheet.instagram_account[0],
                                    sheet.instagram_account[1]
                                ]
  json.info                     [
                                    sheet.emulator,
                                    sheet.health_check
                                ]
end