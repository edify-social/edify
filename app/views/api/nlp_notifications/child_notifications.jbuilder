zone = ActiveSupport::TimeZone['Pacific Time (US & Canada)']
json.notifications @notifs.order('created_at DESC') do |n|
  json.child_phone              n.child.phone

  if n.device_message
    json.id                       n.device_message.id
    json.contact_name             n.device_message.contact_name
    json.contact_number           n.device_message.contact_number
    json.backtrack                n.device_message.backtrack
    json.created_at               Time.parse(n.device_message.mobile_timestamp).strftime("%d %b %Y at %H:%M" + " " + zone.formatted_offset)
  else
    json.id                       n.id
  end

  json.content                  n.content
  json.user                     n.user
  json.label                    n.label
  json.status                   n.status
  json.notification_type        n.notification_type
  json.channel                  n.channel
  json.feedback                 n.feedback
  json.updated_at               n.updated_at.strftime("%d %b %Y at %H:%M" + " " + zone.formatted_offset)
  json.is_active                n.is_active
  json.child_id                 n.child_id
  # Instagram related
  #json.instagram_message_id     n.instagram_message_id

  if n.device_message
    msg_context = n.child.device_messages.where(thread_id: n.device_message.thread_id).where(mobile_timestamp:  (Time.parse(n.device_message.mobile_timestamp)-7.days)..(Time.parse(n.device_message.mobile_timestamp)+7.days)).limit(200)
    json.context msg_context do |msg|
      json.msg_id                     msg.id
      json.msg_user                   msg.contact_number
      json.msg_text                   msg.message
      json.msg_sent                   msg.sent?
      json.msg_created_at             msg.created_at.strftime("%d %b %Y at %H:%M" + " " + zone.formatted_offset)
      # Instagram related
      #  json.msg_instagram_thread_id    msg.instagram_thread_id
      #  json.msg_content                msg.content_type
      #  json.msg_item_type              msg.item_type
      #  json.msg_timestamp              msg.timestamp
      #  json.msg_user                   msg.instagram_users# InstagramUser.find(msg.uid).username
    end
  end

end
