json.resources @resources do |resource|
  json.id         resource.id
  json.link       resource.link
  json.title      resource.title
  json.site       resource.site
  json.logo       resource.logo
  json.image      resource.image
  json.brief      resource.brief
  json.tags       resource.tag_list
end