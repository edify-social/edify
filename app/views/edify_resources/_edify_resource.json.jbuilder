json.extract! edify_resource, :id, :link, :title, :site, :logo, :image, :brief, :created_at, :updated_at
json.url edify_resource_url(edify_resource, format: :json)