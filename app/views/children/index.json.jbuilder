json.array!(@children) do |child|
  json.extract! child, :id, :name, :phone, :imei, :guardian_id
  json.url child_url(child, format: :json)
end
