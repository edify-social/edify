# == Schema Information
#
# Table name: app_lists
#
#  id           :integer          not null, primary key
#  device       :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  child_id     :integer
#  applications :string           default([]), is an Array
#

class AppList < ActiveRecord::Base
  belongs_to :child
end
