# == Schema Information
#
# Table name: child_monitor_settings
#
#  id         :integer          not null, primary key
#  sync_time  :string           default("3600")
#  user       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ChildMonitorSetting < ActiveRecord::Base
  validates_presence_of :sync_time
end
