# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  child_id   :integer
#

class Contact < ActiveRecord::Base
  belongs_to :child
end
