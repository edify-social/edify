# == Schema Information
#
# Table name: instagram_messages
#
#  id                  :integer          not null, primary key
#  content_type        :string
#  status              :string
#  item_type           :string
#  item_id             :string
#  client_context      :string
#  timestamp           :string
#  timestamp_in_micro  :string
#  uid                 :string
#  text                :text
#  preview_medias      :string
#  action_log          :string
#  thread_key          :text             default([]), is an Array
#  hide_in_thread      :boolean
#  instagram_thread_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  sentiment           :string
#

class InstagramMessage < ActiveRecord::Base
  validates_presence_of :item_id, :uid, :timestamp
  validates_uniqueness_of :item_id, case_sensitive: true

  belongs_to :instagram_thread
  has_many :thread_messages, dependent: :destroy
  has_many :instagram_users, through: :thread_messages
  has_many :edify_notifications
end
