# == Schema Information
#
# Table name: nodes
#
#  id                      :integer          not null, primary key
#  aws_id                  :string
#  state                   :string
#  region                  :string
#  architecture            :string
#  hypervisor              :string
#  instance_type           :string
#  key_name                :string
#  launch_time             :string
#  placement               :string
#  private_dns             :string
#  private_ip              :string
#  public_dns              :string
#  public_ip               :string
#  root_device_type        :string
#  root_device_name        :string
#  state_reason            :string
#  state_transition_reason :string
#  subnet_id               :string
#  aws_tags                :text             default([]), is an Array
#  virtualization_type     :string
#  vpc_id                  :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  security_group          :text             default([]), is an Array
#  hostname                :string
#  wake_up                 :datetime
#

class Node < ActiveRecord::Base
  after_create :add_hostname

  has_many :account_sheets
  has_many :children, through: :account_sheets

  def add_hostname
    hn = 'ip'
    hip = self.private_ip
    harray = hip.split('.')
    harray.each do |e|
      hn = hn + '-' + e
    end
    # Depends on hostname
    self.gen_account_sheets(hn)
    self.hostname = hn
    self.save
  end

  def gen_account_sheets(hostname)
    24.times do
      acct = AccountSheet.create(status: 'none', server: hostname)
      self.account_sheets << acct
    end
  end

end
