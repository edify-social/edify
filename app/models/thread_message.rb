# == Schema Information
#
# Table name: thread_messages
#
#  id                   :integer          not null, primary key
#  instagram_message_id :integer
#  instagram_user_id    :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class ThreadMessage < ActiveRecord::Base
  validates_presence_of :instagram_message_id, :instagram_user_id

  belongs_to :instagram_message
  belongs_to :instagram_user
end
