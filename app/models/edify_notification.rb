# == Schema Information
#
# Table name: edify_notifications
#
#  id                   :integer          not null, primary key
#  content              :text
#  user                 :string
#  label                :string
#  status               :string
#  notification_type    :string
#  channel              :string
#  feedback             :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  is_active            :boolean          default(TRUE)
#  instagram_message_id :integer
#  child_id             :integer
#  device_message_id    :integer
#

class EdifyNotification < ActiveRecord::Base
  belongs_to :child
  #belongs_to :instagram_message
  belongs_to :device_message
end
