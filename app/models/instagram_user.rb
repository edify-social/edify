# == Schema Information
#
# Table name: instagram_users
#
#  id                            :integer          not null, primary key
#  username                      :string
#  full_name                     :string
#  profile_pic_url               :string
#  has_anonymous_profile_picture :boolean
#  uid                           :string
#  is_geo_ip_blocked             :boolean
#  is_staff                      :boolean
#  usertag_review_enabled        :boolean
#  biography                     :text
#  external_lynx_url             :string
#  external_url                  :string
#  follower_count                :integer
#  following_count               :integer
#  media_count                   :integer
#  is_private                    :boolean
#  geo_media_count               :integer
#  usertags_count                :integer
#  is_verified                   :boolean
#  byline                        :string
#  chaining_suggestions          :string
#  coeff_weight                  :integer
#  can_see_organic_insights      :boolean
#  inviter_id                    :integer
#  recipient_id                  :integer
#  instagram_message_id          :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#

class InstagramUser < ActiveRecord::Base
  has_many :recipients_assocs
  has_many :instagram_threads, through: :recipients_assocs
  has_many :thread_messages
  has_many :instagram_messages, through: :thread_messages
end
