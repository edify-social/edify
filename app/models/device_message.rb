# == Schema Information
#
# Table name: device_messages
#
#  id               :integer          not null, primary key
#  message          :text
#  contact_name     :string
#  contact_number   :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  child_id         :integer
#  mobile_timestamp :string
#  sent             :boolean
#  thread_id        :integer
#  backtrack        :boolean
#  message_id       :integer
#  raw              :string
#

class DeviceMessage < ActiveRecord::Base
  belongs_to :child

  # Associate multiple messages as with Instagram Messages
  has_many :edify_notifications

  # Scope on recipient list and phone numbers

  scope :sent, -> { where(sent: true) }
  scope :backtracked, -> { where(backtrack: true) }

  def self.notified(cnt = 1)
    select('device_message.*, count(edify_notifications.id) as notifications').joins(:edify_notifications).group('edify_notifications.id').having('count(edify_notifications.id) > ?', 0)
  end

  def notified?
    self.edify_notifications.count > 0
  end

  def self.search(search)
    where("message ILIKE ?", "%#{search}%")
  end

end
