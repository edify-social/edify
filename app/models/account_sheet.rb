# == Schema Information
#
# Table name: account_sheets
#
#  id                :integer          not null, primary key
#  pin               :string
#  last_run          :string
#  status            :string
#  instagram_account :text             default([]), is an Array
#  server            :string
#  emulator          :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  node_id           :integer
#  child_id          :integer
#  health_check      :string
#

class AccountSheet < ActiveRecord::Base
  belongs_to :node
  belongs_to :child
end
