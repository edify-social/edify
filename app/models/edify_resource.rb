# == Schema Information
#
# Table name: edify_resources
#
#  id         :integer          not null, primary key
#  link       :string
#  title      :string
#  site       :string
#  logo       :string
#  image      :string
#  brief      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EdifyResource < ActiveRecord::Base
  acts_as_taggable
  acts_as_taggable_on :topics
end
