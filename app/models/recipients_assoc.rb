# == Schema Information
#
# Table name: recipients_assocs
#
#  id                  :integer          not null, primary key
#  instagram_user_id   :integer
#  instagram_thread_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class RecipientsAssoc < ActiveRecord::Base
  validates_presence_of :instagram_thread_id, :instagram_user_id

  belongs_to :instagram_user
  belongs_to :instagram_thread
end
