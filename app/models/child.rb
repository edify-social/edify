# == Schema Information
#
# Table name: children
#
#  id                  :integer          not null, primary key
#  name                :string
#  phone               :string
#  imei                :string
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  pin                 :string
#  instagram           :text             default([]), is an Array
#  image               :text
#  backtrack_status    :integer          default(0), not null
#  last_device_checkin :datetime
#  device_connected    :boolean          default(TRUE)
#

class Child < ActiveRecord::Base
  validates_presence_of :name, :user_id
  after_create :choose_node

  # Backtrack Status
  # 0 = incomplete
  # 1 = complete

  mount_uploader :image, AvatarUploader
  mount_base64_uploader :image, AvatarUploader

  belongs_to :user
  has_many :child_thread
  has_many :instagram_threads, through: :child_thread

  #has_one :app_list
  has_many :app_list
  has_many :device_messages

  has_many :account_sheets
  has_many :nodes, through: :account_sheets

  has_many :edify_notifications

  has_many :contacts

  def set_pin
    pin = rand(1e7...1e8).to_i
    self.pin = pin
    self.save
  end

  def choose_node
    unless self.pin
      self.set_pin
    end
    nodes = Node.where('aws_id  != ? AND state = ?', 'i-eaded85f', 'running')
    nodes.each do |n|
      accts = n.account_sheets.where(status: 'none')
      available_emu = accts.first
      if available_emu != nil
        # Assign to emulator
        # instagram[0] = username; instagram[1] = password;
        puts 'Assigning child to node.'
        available_emu.status = 'assigned'
        available_emu.instagram_account[0] = self.instagram[0]
        available_emu.instagram_account[1] = self.instagram[1]
        available_emu.child = self
        available_emu.pin = self.pin
        available_emu.save
        # else
        #  puts 'No available node yet.'
        #  # Create Node
        #  ProvisionInstanceWorker.perform_async(self.id)
      end
      break
    end
  end

end
