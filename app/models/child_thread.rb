# == Schema Information
#
# Table name: child_threads
#
#  id                  :integer          not null, primary key
#  child_id            :integer
#  instagram_thread_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class ChildThread < ActiveRecord::Base
  belongs_to :child
  belongs_to :instagram_thread
end
