# == Schema Information
#
# Table name: aws_regions
#
#  id         :integer          not null, primary key
#  name       :string
#  code       :string
#  active     :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AwsRegion < ActiveRecord::Base
end
