# == Schema Information
#
# Table name: instagram_threads
#
#  id            :integer          not null, primary key
#  thread_id     :string
#  thread_title  :string
#  string        :string
#  muted         :boolean
#  named         :boolean
#  canonical     :boolean
#  last_activity :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class InstagramThread < ActiveRecord::Base
  validates_presence_of :thread_id
  validates_uniqueness_of :thread_id, case_sensitive: true

  has_many :child_threads
  has_many :children, through: :child_threads

  has_many :instagram_messages, dependent: :destroy
  has_many :recipients_assocs, dependent: :destroy
  has_many :instagram_users, through: :recipients_assocs
end
