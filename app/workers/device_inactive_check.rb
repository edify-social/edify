require 'sidekiq-scheduler'
require 'active_support/time'

class DeviceInactiveCheckWorker
  include Sidekiq::Worker

  SAFE_TIME = 12.hours

  def perform
    children = Child.all
    children.each do |ch|

      # Previous conected state
      connected = ch.device_connected

      if ch.last_device_checkin
        # Check if device is connected
        if ch.last_device_checkin + SAFE_TIME < DateTime.now
          ch.device_connected = false
          ch.save

          # Check if its the first time we notice the device to be disconnected
          if connected != ch.device_connected
            # Send Push Notifcation
            params = {
              app_id: '8c010546-76ff-40f6-8761-5163eb01f8dd',
              contents:
                {
                  en: "#{ch.name}'s device has been inactive for more than 12 hours"
                },
              ios_badgeType: 'Increase',
              ios_badgeCount: 1,
              include_player_ids: [ch.user.player_id]
            }
            uri = URI.parse('https://onesignal.com/api/v1/notifications')
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true

            request = Net::HTTP::Post.new(uri.path,
                                          'Content-Type'  => 'application/json;charset=utf-8',
                                          'Authorization' => 'Basic ZGM4MDUxMjgtMzZlMi00MjJkLWE4NjgtZTNjYTc2YjVjMTc4')
            request.body = params.as_json.to_json
            http.request(request)

            channel = 'Inactivity Alert'
            label = 'Warning'
            content = "#{ch.name}'s device has been inactive since #{ch.last_device_checkin}"

            # Create an EdifyNotification
            notification(ch.id, channel, label, content, ch.user)
          end
        else
          ch.device_connected = true
          ch.save
        end
      else
        puts 'Child device has never connected to server.'
      end
    end
  end

  private

  def notification(ch_id, channel, label, content, usr)
    EdifyNotification.create(
      child_id: ch_id,
      channel: channel,
      device_message_id: 0,
      notification_type: 'Device',
      label: label,
      content: content,
      user: usr
    )
  end

end
