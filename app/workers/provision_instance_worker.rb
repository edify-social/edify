class ProvisionInstanceWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(ch_id)

    if Rails.env.development?
      region      = 'us-east-2'
      ami_id      = 'ami-d1cb91b4'
      key_name    = 'edify_emulators_dev'
      sec_group   = 'sg-5499263d'
      av_zone     = 'us-east-2a'
      subnet      = 'subnet-2a9d3443'
      env         = 'Dev'
    else
      region      = 'us-west-2'
      ami_id      = 'ami-b7a114d7'
      key_name    = 'edify-emulators'
      sec_group   = 'sg-31ebdc54'
      av_zone     = 'us-west-2a'
      subnet      = 'subnet-79cd6f0e'
      env         = 'Staging'
    end

    ec2 = Aws::EC2::Resource.new(region: region)

    instance = ec2.create_instances({
        image_id: ami_id,
        min_count: 1,
        max_count: 1,
        key_name: key_name,
        security_group_ids: [sec_group],
        instance_type: 't2.medium',
        placement: {
            availability_zone: av_zone
        },
        subnet_id: subnet,
        block_device_mappings: [
            {
                device_name: '/dev/sda1',
                ebs: {
                    volume_size: 60,
                    delete_on_termination: true
                }
            }
        ]
    })

    # Wait for the instance to be created, running, and passed status checks
    ec2.client.wait_until(:instance_status_ok, {instance_ids: [instance[0].id]})

    # Get info from recently created instance
    i = ec2.instance(instance[0].id)

    # Update infra and check assign ASheet to child
    inst_id = create_node(i, region)

    # Add tags to instance
    instance.batch_create_tags(
        { tags: [
            { key: 'Name', value: 'Emulator-' + inst_id.to_s },
            { key: 'Environment', value: env }
        ]})

    # If child is waiting for new server send back to test for this new one
    child_id = ch_id || 0
    if child_id
      if child_id > 0
        child = Child.find(child_id)
        child.choose_node
      end
    end

    # Bootstrap with Knife
    BootstrapInstanceWorker.perform_async(inst_id, i.public_ip_address)
  end

  def create_node(inst, reg)
    new_node = Node.new
    new_node.aws_id                     = inst.id
    new_node.state                      = inst.state.name
    new_node.region                     = reg
    new_node.architecture               = inst.architecture
    new_node.hypervisor                 = inst.hypervisor
    new_node.instance_type              = inst.instance_type
    new_node.key_name                   = inst.key_name
    new_node.launch_time                = inst.launch_time
    new_node.placement                  = inst.placement.availability_zone
    new_node.private_dns                = inst.private_dns_name
    new_node.private_ip                 = inst.private_ip_address
    new_node.public_dns                 = inst.public_dns_name
    new_node.public_ip                  = inst.public_ip_address
    new_node.root_device_type           = inst.root_device_type
    new_node.root_device_name           = inst.root_device_name
    inst.security_groups.each_with_index do |sg, idx|
      new_node.security_group[idx] = sg['group_name']
    end
    new_node.state_reason               = inst.state_reason
    new_node.state_transition_reason    = inst.state_transition_reason
    new_node.subnet_id                  = inst.subnet_id
    inst.tags.each_with_index do |tg, ix|
      new_node.aws_tags[ix] = [tg['key'], tg['value']]
    end
    new_node.virtualization_type        = inst.virtualization_type
    new_node.vpc_id                     = inst.vpc_id
    new_node.save

    return new_node.id
  end

end
