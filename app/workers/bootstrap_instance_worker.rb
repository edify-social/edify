class BootstrapInstanceWorker
  include Chef::Knife::API
  include Sidekiq::Worker

  def perform(n_id, pub_ip)

    n = Node.find(n_id)
    emu_name = 'Emulator-' + n_id.to_s

    # Bootstrap with Knife
    if Rails.env.development?
      key_file = '~/.ssh/edify_emulators_dev.pem'
    else
      key_file = '~/.ssh/edify-emulators.pem'
    end

    knife :bootstrap, ['-y', pub_ip, '-N', emu_name , '-x', 'ubuntu', '--sudo', '--i', key_file]

    # On workstation each cookbook should have been uploaded already with:
    # knife cookbook upload <name-of-cookbook>

    # Add to run list: cron and delvaditate, VNC,
    # Chef command knife node run_list add <emulator-name> <recipe>
    knife :node_run_list_add, [
        emu_name,
        'recipe[cron-delvalidate::default]',
        'recipe[vnc::default]',
        'recipe[android-sdk::default]'
    ]

    # Pending: Run Emulator Python init Script Cookbook
    # /usr/local/android-sdk/tools/android list targets
    # If emulator -list-avds is empty then run
    # echo "n" | android create avd --name myandroid22 -t "android-22" --abi "default/armeabi-v7a"
    # now run emulator -avd ARMV7
    # Reported error Could not create SDL2 window: GLX is not supported

    # Execute chef-client on node
    # Chef command knife ssh 'name:<emulator-name>' -x ubuntu -i ~/.ssh/edify-emulators.pem 'sudo chef-client'
    knife :ssh, [ 'name:' + emu_name, '-x', 'ubuntu', '-i', key_file, 'sudo chef-client']
  end
end