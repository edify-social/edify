class TerminateInstanceWorker
  include Sidekiq::Worker

  def perform(inst_aws_id)

    node = Node.find_by_aws_id(inst_aws_id)
    inst_aws_region = node.region

    ec2 = Aws::EC2::Resource.new(region: inst_aws_region)

    i = ec2.instance(inst_aws_id)

    if i.exists?
      case i.state.code
        when 48  # terminated
          puts "#{inst_aws_id} is already terminated"
        else
          i.terminate
      end
    end

    # Remove local record
    node.destroy
  end
end
