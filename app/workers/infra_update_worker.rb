class InfraUpdateWorker
  include Sidekiq::Worker
  include SidekiqStatus::Worker

  def perform

    self.total = 100
    at 0

    active_regions = AwsRegion.where(active: true)

    active_regions.each_with_index do |r, i|
      ec2 = Aws::EC2::Resource.new(region: r.code)
      aws_instances = ec2.instances

      at ((i.to_f/active_regions.count) * 100.00)

      aws_instances.each do |awsi|
        node = Node.find_by_aws_id(awsi.id)
        if !node && awsi.state.name == 'running'
          new_node = Node.new
          new_node.aws_id                     = awsi.id
          new_node.state                      = awsi.state.name
          new_node.region                     = r.code
          new_node.architecture               = awsi.architecture
          new_node.hypervisor                 = awsi.hypervisor
          new_node.instance_type              = awsi.instance_type
          new_node.key_name                   = awsi.key_name
          new_node.launch_time                = awsi.launch_time
          new_node.placement                  = awsi.placement.availability_zone
          new_node.private_dns                = awsi.private_dns_name
          new_node.private_ip                 = awsi.private_ip_address
          new_node.public_dns                 = awsi.public_dns_name
          new_node.public_ip                  = awsi.public_ip_address
          new_node.root_device_type           = awsi.root_device_type
          new_node.root_device_name           = awsi.root_device_name
          awsi.security_groups.each_with_index do |sg, idx|
            new_node.security_group[idx] = sg['group_name']
          end
          new_node.state_reason               = awsi.state_reason
          new_node.state_transition_reason    = awsi.state_transition_reason
          new_node.subnet_id                  = awsi.subnet_id
          awsi.tags.each_with_index do |tg, ix|
            new_node.aws_tags[ix] = [tg['key'], tg['value']]
          end
          new_node.virtualization_type        = awsi.virtualization_type
          new_node.vpc_id                     = awsi.vpc_id
          new_node.save

        elsif node && node.state == 'terminated'
          node.destroy

        elsif node && node.state != 'terminated'
          node.aws_id                     = awsi.id
          node.state                      = awsi.state.name
          node.region                     = r.code
          node.architecture               = awsi.architecture
          node.hypervisor                 = awsi.hypervisor
          node.instance_type              = awsi.instance_type
          node.key_name                   = awsi.key_name
          node.launch_time                = awsi.launch_time
          node.placement                  = awsi.placement.availability_zone
          node.private_dns                = awsi.private_dns_name
          node.private_ip                 = awsi.private_ip_address
          node.public_dns                 = awsi.public_dns_name
          node.public_ip                  = awsi.public_ip_address
          node.root_device_type           = awsi.root_device_type
          node.root_device_name           = awsi.root_device_name
          awsi.security_groups.each_with_index do |sg, idx|
            node.security_group[idx] = sg['group_name']
          end
          node.state_reason               = awsi.state_reason
          node.state_transition_reason    = awsi.state_transition_reason
          node.subnet_id                  = awsi.subnet_id
          awsi.tags.each_with_index do |tg, ix|
            node.aws_tags[ix] = [tg['key'], tg['value']]
          end
          node.virtualization_type        = awsi.virtualization_type
          node.vpc_id                     = awsi.vpc_id
          node.save
        end

      end

    end

    at 100
  end

end
