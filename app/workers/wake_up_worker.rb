class WakeUpWorker
  include Sidekiq::Worker

  def perform(*args)
    nodes = Node.where('aws_id  != ? AND state = ?', 'i-eaded85f', 'stopped')
    nodes.each do |n|
      if n.wake_up
        if n.wake_up <= Time.now
          ec2 = Aws::EC2::Resource.new(region: n.region)
          i = ec2.instance(n.aws_id)
          inst = n.aws_id
          if i.exists?
            case i.state.code
              when 0  # pending
                puts "#{inst} is pending, so it will be running in a bit"
              when 16  # started
                puts "#{inst} is already started"
              when 48  # terminated
                puts "#{inst} is terminated, so you cannot start it"
              else
                i.start
            end
          end
          InfraUpdateWorker.perform_async
        end
      end
    end

  end

end
