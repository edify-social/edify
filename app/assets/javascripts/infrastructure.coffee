# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
queryForPercentage = () ->
  job_id = $('#job_id').text()

  $.ajax({
    url: "/percentage_done"
    data:
      job_id: job_id
    success: (data) ->
      percentage = 'width: ' + data['percentage_done'] + '%;'

      # Writing the percentage done to the progress bar
      $('#job-progress').attr('style', percentage).text(data['percentage_done'] + '%')

      if $('#job-progress').text() != '100%'
        setTimeout(queryForPercentage, 1500)
      else
        window.location.reload()
  })

$ ->

  $('#job-id-container').bind('DOMSubtreeModified', queryForPercentage )
  $('.sync-btn').click ->
    $('.sync-btn').hide()
    return