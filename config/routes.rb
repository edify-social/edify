Rails.application.routes.draw do

  resources :nlp_dictionaries
  get '/additional_meanings', to: 'nlp_dictionaries#index_add_mean'
  get '/additional_meanings/new', to: 'nlp_dictionaries#new_additional_meaning', as: 'new_additional_meaning'

  require 'sidekiq/web'
  require 'sidekiq_status/web'

  devise_for :users

  #Redirect if authenticated
  devise_scope :user do
    # Root for auth/no auth user
    authenticated :user do
      #if
      root 'dashboard#show', as: :authenticated_root
    end
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
    scope '/admin' do
      resources :users
      resources :children
      resources :insta_data
      resources :edify_notifications
      resources :edify_resources
      resources :infrastructure
      resources :aws_regions
      get     'child_notifications/:id',           to: 'children#notifications',           as: 'child_notifications'
      get     'child_device_messages/:id',         to: 'children#device_messages',         as: 'child_device_messages'
      get     'child_device_message_thread/:id/:thread_id',  to: 'children#device_message_thread',   as: 'child_device_message_thread'
      get     'child_device_messages_table/:id',   to: 'children#device_messages_table',   as: 'child_device_messages_table'
      get     'backtracked_thread/:id/:thread_id', to: 'children#backtracked_thread',      as: 'backtracked_thread'
      get     'backtracked_table/:id',             to: 'children#backtracked_table',       as: 'backtracked_table'
      post    'stop_node',                         to: 'infrastructure#stopnode'
      post    'start_node',                        to: 'infrastructure#startnode'
      post    'provision_node',                    to: 'infrastructure#provision_node'
      post    'terminate_node',                    to: 'infrastructure#terminate_node'
      get     'account_sheets/:id',                to: 'account_sheets#show',              as: 'show_account_sheets'
      get     'account_sheets/:id/edit',           to: 'account_sheets#edit',              as: 'edit_account_sheets'
      put     'account_sheets/:id',                to: 'account_sheets#update',            as: 'account_sheet'
      patch   'account_sheets/:id',                to: 'account_sheets#update',            as: 'account_sheets'
      delete  'infrastructure/delete/:id',         to: 'infrastructure#destroy',           as: 'node_destroy'

      # Admin Dashboard
      get '/dashboard/instagram_user_index',       as: 'instagram_users'
      get '/dashboard/instagram_messages_index',   as: 'instagram_messages'
      get '/dashboard/instagram_user_show/:id',    to: 'dashboard#instagram_user_show',    as: 'instagram_user'
      get '/dashboard/recipients_index/:id',       to: 'dashboard#recipients_index',       as: 'recipients'
      get '/dashboard/thread_messages_index/:id',  to: 'dashboard#thread_messages_index',  as: 'thread_msgs'
      get '/dashboard/api',                        to: 'dashboard#api_docs',               as: 'api'
      get '/dashboard/api_editor',                 to: 'dashboard#api_editor',             as: 'api_editor'
      get '/dashboard/language_model',             to: 'dashboard#language_model',         as: 'language_model'
      get '/dashboard/log_monitoring',             to: 'dashboard#log_monitoring',         as: 'log_monitor'
      get '/dashboard/mobile_apps',                to: 'dashboard#mobile_apps',            as: 'mobile_app'
      get '/dashboard/insta_msg_nlp',              to: 'dashboard#insta_msg_nlp',          as: 'msg_nlp'
      delete  'remove_thread',                     to: 'children#remove_child_thread',     as: 'remove_child_thread'
      delete  'remove_device_message',             to: 'children#remove_device_message',   as: 'remove_device_message'
      delete  'remove_notification',               to: 'children#remove_notification',     as: 'remove_notification'
      post 'notify/:id',                           to: 'dashboard#notify',                 as: 'notify'
    end
  end

  # Background job ajax request
  post 'infrastructure/sync',                  to: 'infrastructure#infra_sync',        as: 'infra_sync'
  get 'percentage_done',                       to: 'infrastructure#percentage_done',   as: 'percentage_done'
  get '/dashboard/background_jobs',            to: 'dashboard#background_jobs'

  # General Dashboard
  get '/dashboard',                            to: 'dashboard#show',                   as: 'dashboard'

  # API
  constraints subdomain: 'api' do
    namespace :api, path: '/', :defaults => {:format => :json} do
      scope :v1 do
        # Users
        devise_scope :user do
          post    'registrations',  to: 'users#create',     as: 'register'
          post    'sessions',       to: 'sessions#create',  as: 'login'
          delete  'sessions',       to: 'sessions#destroy', as: 'logout'
        end

        patch   '/user/update/:id',                   to: 'users#update',                           as: 'api_update_users'
        get     '/user/:id',                          to: 'users#show',                             as: 'api_user'
        get     '/users',                             to: 'users#index',                            as: 'api_users'
        post    'register_push',                      to: 'users#register_for_push',                as: 'register_push'
        post    'purchase_backtrack',                 to: 'users#purchase_backtrack',               as: 'purchase_backtrack'
        post    'child_onboarding/add_child',         to: 'child_onboarding#add_child_account',     as: 'add_child_account'
        delete  'child_onboarding/remove_child',      to: 'child_onboarding#remove_child_account',  as: 'remove_child_account'
        post    'insta_data/incoming',                to: 'insta_data#incoming',                    as: 'instafeed'
        post    'child_onboarding/mobile_profile',    to: 'child_onboarding#mobile_profile',        as: 'mobile_profile'
        post    'child_onboarding/activate_android',  to: 'child_onboarding#activate_android',      as: 'activate_android'
        post    'child_onboarding/activate_ios',      to: 'child_onboarding#activate_ios',          as: 'activate_ios'
        post    'insta_data/incoming',                to: 'insta_data#incoming',                    as: 'instagram_incoming'
        post    'insta_data/main_feed',               to: 'insta_data#main_feed',                   as: 'instagram_main_feed'
        get     'chmon_ctrl/sync_time',               to: 'child_monitor_settings#sync_time',       as: 'sync_time'
        get     'resources',                          to: 'edify_resources#index',                  as: 'edify_resources'
        post    'notification_poll',                  to: 'nlp_notifications#child_notifications',  as: 'notification_poll'
        post    'update_notifications',               to: 'nlp_notifications#update_notification',  as: 'update_notification'
        post    'device_messages',                    to: 'child_mobile_data#upload_messages'
        post    'backtrack',                          to: 'child_mobile_data#backtrack'
        post    'backtrack_check',                    to: 'child_mobile_data#backtrack_check'
        get     'device_messages',                    to: 'child_mobile_data#device_messages'
        post    'request_backtrack',                  to: 'child_mobile_data#request_backtrack'
        post    'app_list',                           to: 'child_mobile_data#upload_app_list'
        post    'contacts',                           to: 'child_mobile_data#upload_contacts'
        get     'contacts',                           to: 'child_mobile_data#contacts'
        get     'app_list',                           to: 'child_mobile_data#app_list'
        get     'last_message',                       to: 'child_mobile_data#last_message_saved'
        post    'account_sheets',                     to: 'account_sheets#emulator_accounts'
        post    'upload',                             to: 'child_onboarding#add_avatar',            as: 'upload_avatar'
        post    'update_instagram',                   to: 'child_onboarding#update_instagram',      as: 'update_instagram'
        post    'update_child',                       to: 'child_onboarding#update_child',          as: 'update_child'
        post    'update_user',                        to: 'users#update',                           as: 'update_user'
        post    'check_connection',                   to: 'users#check_device_connection',          as: 'check_connection'
      end
    end
  end

end
